package cw1_processing;


/**
 * Generates the start string to run processing window
 * @author Mark
 */
public class RunString {
    
    /**
     * Returns the required string to run the processing window
     * @param cameraNumber  Select the camera which will be used, currently only 2 cameras are supported thus the valid input is (1 or 2)
     * @return 
     */
    public static String[] get(int cameraNumber){
        boolean present = Constants.getInstance().presentationMode;
        switch(cameraNumber){
            case 0:
                if(present){
                    return new String[]{"--present", Constants.initProcessingClass, Constants.getInstance().cameraOnePort, ""+cameraNumber};
                }else{
                    return new String[]{Constants.initProcessingClass, Constants.getInstance().cameraOnePort, ""+cameraNumber};
                }
            case 1:
                if(present){
                    return new String[]{"--present", Constants.initProcessingClass, Constants.getInstance().cameraTwoPort, ""+cameraNumber};
                }else{
                    return new String[]{Constants.initProcessingClass, Constants.getInstance().cameraTwoPort, ""+cameraNumber};
                }
            default:
                System.out.print("Not Supported");
                return null;
        }
    }
}
