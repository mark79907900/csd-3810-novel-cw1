package cw1_processing;

import cw1_processing.BusinessLogic.Config;

/**
 * This Class holds the constants in the Used in this project
 *
 * @author Mark
 */
public class Constants {

    private static volatile Constants self = null;

    //Config file constants
    public static String ConfigFileLocation = "config.properties";

    //run processing
    public static String initProcessingClass;

    //Constants
    public final int threadPoolSize;
    public final String arduinoPort;
    public final String avatarImagesLocation;
    public final boolean waitForArduino;
    public final String cameraOnePort;
    public final String cameraTwoPort;
    public final long setupDelay;
    public final long gameStartDelay;
    public final boolean presentationMode;
    public final boolean screenOrientationPortriat;
    
    public final String avatarIDList;
    public final int avatarSelectorSize;
    public final int avatarSelectorfiducialIdMultiplier;
    public final int avatarIconsOffsetFromSelector;
    public final int avatarIconsOffsetFromEachOther;
    public final float avatarRotateArrowOffsetFromScreenY;
    public final boolean avatarShowSelectorCircle;
    public final float avatarSelectorSwipeLength;
    public final float avatarRotationMinFadeValue;
    public final float avatarRotationMaxFadeValue;
    public final float avatarRotationIncrementRate;
    public final long avatarRotationDelayIncrement;
    public final boolean avatarRotationFadeBothDirections;
    public final long avatarRotationFadeNumberOfFullFadeCycles;

    //public final float avatarSwipeMinFadeValue;
    //public final float avatarSwipeMaxFadeValue;
    //public final float avatarSwipeIncrementRate;
    //public final long avatarSwipeDelayIncrement;
    //public final boolean avatarSwipeFadeBothDirections;
    //public final long avatarSwipeFadeNumberOfFullFadeCycles;
    public final String arrowImagesLocation;
    public final String avatarRotationArrowImage;
    public final String avatarSwipeArrowImage;
    public final long avatarSwipeImageDelay;
    public final long avatarRotateImageDelay;
    public final String crayonsImagesLocation;
    public final String textImagesLocation;
    public final String placeSelectorTextImage;
    public final String selectorSwipeUpTextImage;
    public final String selectorRotateTextImage;
    public final String waitingForOtherPlayerTextImage;
    public final String otherImagesLocation;
    public final String timerImage;
    public final String timerExpiringImage;
    public final int fps;
    public final long countdownEffect;
    public final long duration;
    public final float timerMinFadeValue;
    public final float timerMaxFadeValue;
    public final float timerFadeIncrementRate;
    public final long timerFadeDelayIncrement;
    public final boolean timerFadeBothDirections;
    public final long timerFadeNumberOfFullFadeCycles;
    
    public final float avatarSwipeTransitionOpacity;
    public final int avatarSwipeTransitionNumOfImages;
    public final long avatarSwipeTransitionDelay;
    public final int avatarSwipeTransitionLoopFor;
    public final String sketchesInfo;
    public final String gameStateImageGameOver;
    public final String gameStateImageWin;
    public final String gameStateImageTimeUp;
    
    public final int numberOfPlayers;
    
    public final int angleOffset;
    public final String woodenBlockSoundLocation;
    
    private Constants() {
        //hardcoded constants
        initProcessingClass = "cw1_processing.BusinessLogic.Threads.appMain";

        //constants
        threadPoolSize = retrieveIntFromConf("threadPoolSize");
        arduinoPort = retrieveStringFromConf("arduinoPort");
        fps = retrieveIntFromConf("fps");
        duration = retrieveLongFromConf("duration");
        countdownEffect = retrieveLongFromConf("countdownEffect");
        sketchesInfo = retrieveStringFromConf("sketchesInfoLocation");
        gameStartDelay = retrieveLongFromConf("gameStartDelay");

        //general
        waitForArduino = retrieveBooleanFromConf("waitForArduino");
        cameraOnePort = retrieveStringFromConf("cameraOnePort");
        cameraTwoPort = retrieveStringFromConf("cameraTwoPort");
        setupDelay = retrieveLongFromConf("setupDelay");
        presentationMode = retrieveBooleanFromConf("presentationMode");
        woodenBlockSoundLocation = retrieveStringFromConf("woodenBlockSoundLocation");

        //Images
        avatarImagesLocation = retrieveStringFromConf("avatarImagesLocation");
        arrowImagesLocation = retrieveStringFromConf("arrowImagesLocation");
        crayonsImagesLocation = retrieveStringFromConf("crayonsImagesLocation");
        textImagesLocation = retrieveStringFromConf("textImagesLocation");
        otherImagesLocation = retrieveStringFromConf("otherImagesLocation");
        avatarRotationArrowImage = retrieveStringFromConf("avatarRotationArrowImage");
        avatarSwipeArrowImage = retrieveStringFromConf("avatarSwipeArrowImage");
        placeSelectorTextImage = retrieveStringFromConf("placeSelectorTextImage");
        selectorSwipeUpTextImage = retrieveStringFromConf("selectorSwipeUpTextImage");
        selectorRotateTextImage = retrieveStringFromConf("selectorRotateTextImage");
        waitingForOtherPlayerTextImage = retrieveStringFromConf("waitingForOtherPlayerTextImage");
        timerImage = retrieveStringFromConf("timerImage");
        timerExpiringImage = retrieveStringFromConf("timerExpiringImage");
        gameStateImageGameOver = retrieveStringFromConf("gameStateImageGameOver");
        gameStateImageWin = retrieveStringFromConf("gameStateImageWin");
        gameStateImageTimeUp = retrieveStringFromConf("gameStateImageTimeUp");

        //orientation
        screenOrientationPortriat = retrieveBooleanFromConf("screenOrientationPortriat");

        //avatar selector
        avatarIDList = retrieveStringFromConf("avatarIDList");
        avatarSelectorSize = retrieveIntFromConf("avatarSelectorSize");
        avatarSelectorfiducialIdMultiplier = retrieveIntFromConf("avatarSelectorfiducialIdMultiplier");
        avatarIconsOffsetFromSelector = retrieveIntFromConf("avatarIconsOffsetFromSelector");
        avatarIconsOffsetFromEachOther = retrieveIntFromConf("avatarIconsOffsetFromEachOther");
        avatarSelectorSwipeLength = retrieveFloatFromConf("avatarSelectorSwipeLength");
        avatarRotateArrowOffsetFromScreenY = retrieveFloatFromConf("avatarRotateArrowOffsetFromScreenY");
        avatarShowSelectorCircle = retrieveBooleanFromConf("avatarShowSelectorCircle");

        //rotation fader
        avatarRotationMinFadeValue = retrieveFloatFromConf("avatarRotationMinFadeValue");
        avatarRotationMaxFadeValue = retrieveFloatFromConf("avatarRotationMaxFadeValue");
        avatarRotationIncrementRate = retrieveFloatFromConf("avatarRotationIncrementRate");
        avatarRotationDelayIncrement = retrieveLongFromConf("avatarRotationDelayIncrement");
        avatarRotationFadeBothDirections = retrieveBooleanFromConf("avatarRotationFadeBothDirections");
        avatarRotationFadeNumberOfFullFadeCycles = retrieveLongFromConf("avatarRotationFadeNumberOfFullFadeCycles");

        //swipe fader
//        avatarSwipeMinFadeValue = retrieveFloatFromConf("avatarSwipeMinFadeValue");
//        avatarSwipeMaxFadeValue = retrieveFloatFromConf("avatarSwipeMaxFadeValue");
//        avatarSwipeIncrementRate = retrieveFloatFromConf("avatarSwipeIncrementRate");
//        avatarSwipeDelayIncrement = retrieveLongFromConf("avatarSwipeDelayIncrement"));
//        avatarSwipeFadeBothDirections = retrieveBooleanFromConf"avatarSwipeFadeBothDirections"));
//        avatarSwipeFadeNumberOfFullFadeCycles = retrieveLongFromConf("avatarSwipeFadeNumberOfFullFadeCycles"));
        //swipe transition
        avatarSwipeTransitionOpacity = retrieveFloatFromConf("avatarSwipeTransitionOpacity");
        avatarSwipeTransitionNumOfImages = retrieveIntFromConf("avatarSwipeTransitionNumOfImages");
        avatarSwipeTransitionDelay = retrieveLongFromConf("avatarSwipeTransitionDelay");
        avatarSwipeTransitionLoopFor = retrieveIntFromConf("avatarSwipeTransitionLoopFor");

        //delay to show arrows
        avatarSwipeImageDelay = retrieveLongFromConf("avatarSwipeImageDelay");
        avatarRotateImageDelay = retrieveLongFromConf("avatarRotateImageDelay");

        //timer
        timerMinFadeValue = retrieveFloatFromConf("timerMinFadeValue");
        timerMaxFadeValue = retrieveFloatFromConf("timerMaxFadeValue");
        timerFadeIncrementRate = retrieveFloatFromConf("timerFadeIncrementRate");
        timerFadeDelayIncrement = retrieveLongFromConf("timerFadeDelayIncrement");
        timerFadeBothDirections = retrieveBooleanFromConf("timerFadeBothDirections");
        timerFadeNumberOfFullFadeCycles = retrieveLongFromConf("timerFadeNumberOfFullFadeCycles");

        //Players
        numberOfPlayers = retrieveIntFromConf("numberOfPlayers");

        //shapes
        angleOffset = retrieveIntFromConf("angleOffset");
    }
    
    public static void init() {
        if (Constants.self == null) {
            Constants.self = new Constants();
        }
    }
    
    public static Constants getInstance() {
        init();
        return Constants.self;
    }
    
    private String retrieveStringFromConf(String propertyName) {
        return Config.getProperty(propertyName);
    }
    
    private int retrieveIntFromConf(String propertyName) {
        return Integer.parseInt(Config.getProperty(propertyName));
    }
    
    private float retrieveFloatFromConf(String propertyName) {
        return Float.parseFloat(Config.getProperty(propertyName));
    }
    
    private long retrieveLongFromConf(String propertyName) {
        return Long.parseLong(Config.getProperty(propertyName));
    }
    
    private double retrieveDoubleFromConf(String propertyName) {
        return Double.parseDouble(Config.getProperty(propertyName));
    }
    
    private boolean retrieveBooleanFromConf(String propertyName) {
        return Boolean.parseBoolean(Config.getProperty(propertyName));
    }
}
