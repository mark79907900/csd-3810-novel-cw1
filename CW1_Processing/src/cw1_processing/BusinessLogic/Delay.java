/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw1_processing.BusinessLogic;

/**
 *
 * @author Mark
 */
public class Delay {
    private long start;

    public Delay() {
        this.start = System.currentTimeMillis();
    }
    
    public void start(){
        this.start = System.currentTimeMillis();
    }
    
    public boolean action(long diff){
        return (System.currentTimeMillis() - this.start >= diff);
    }
    
    public boolean actionAndUpdate(long diff){
        if(System.currentTimeMillis() - this.start >= diff){
            start();
            return true;
        }
        return false;
    }
}
