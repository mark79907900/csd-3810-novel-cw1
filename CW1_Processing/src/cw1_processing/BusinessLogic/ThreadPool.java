/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package cw1_processing.BusinessLogic;

import cw1_processing.Constants;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * It will hold the threads and related operations
 * @author Mark
 */
public class ThreadPool {
    private static ThreadPool self = null;
    private static ExecutorService executor;
    private int threadPoolSize;
    
    /**
     * Initialise the thread pool
     */
    public static void init(){
        if(ThreadPool.self == null){
            ThreadPool.self = new ThreadPool();
        }
    }
    
    /**
     * Private constructor
     */
    private ThreadPool(){
        this.threadPoolSize = Constants.getInstance().threadPoolSize;
        ThreadPool.executor = Executors.newFixedThreadPool(this.threadPoolSize);
    }
    
    /**
     * Push a Thread in the thread pool. 
     * If the thread pool has not yet been initialised, it will automatically get initialised
     * @param thread 
     */
    public static void pushThread(Runnable thread){
        init();
        ThreadPool.executor.submit(thread);
    }
}
