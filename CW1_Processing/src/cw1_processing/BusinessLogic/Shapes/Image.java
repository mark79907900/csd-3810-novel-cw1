package cw1_processing.BusinessLogic.Shapes;

import fisica.FBox;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;

/**
 * This class is used to add images, it has these additional features:
 * Tint (opacity)
 * @author Mark
 */
public class Image extends FBox{
    
    private final PApplet parent;
    private final PImage image;
    private final float imageWidth;
    private final float imageHeight;
    private float tint;
    
    public Image(PApplet parent, String imagePath) {
        super(0,0);
        this.parent = parent;
        this.image = this.parent.loadImage(imagePath);
        this.imageWidth = -(this.image.width/2);
        this.imageHeight = -(this.image.height/2);
        this.tint = 255;
    }
    
    @Override
    public void draw(PGraphics applet) {
        super.draw(applet);
        super.setStatic(true);
        super.setBullet(true);
        super.setNoStroke();
        preDraw(applet);
        this.parent.fill(0);
        this.parent.tint(255, this.tint);
        this.parent.image(this.image, this.imageWidth, this.imageHeight);
        postDraw(applet);
    }
    
    /**
     * Change the opacity of the image
     * @param tint
     */
    public void tint(float tint){
        this.tint = tint;
    }
    
    public float getImageWidth(){
        return this.image.width;
    }
    
    public float getImageHeight(){
        return this.image.height;
    }
    
}
