package cw1_processing.BusinessLogic.Shapes;

import fisica.FBody;
import fisica.FBox;
import processing.core.PApplet;
import processing.core.PGraphics;

public class FSemiCircle extends FBox {
    private float radius;
    private PApplet parent;
    private float r,g,b;
    
    public FSemiCircle(PApplet parent, float radius) {
        super(radius, radius/2);
        this.radius = radius;
        this.parent = parent;
        this.r = 0;
        this.g = 0;
        this.b = 0;
    }
    
    @Override
    public void draw(PGraphics applet) {
        super.draw(applet);
        super.setStatic(true);
        super.setBullet(true);
//        super.setNoStroke();
        super.setFill(0);
//        super.setPosition(getX(), getY()+this.radius/4);
        
        preDraw(applet);
        this.parent.fill(r,g,b);
        this.parent.rotate((float)Math.PI);
        this.parent.arc(0, 0, this.radius, this.radius, 0, (float) Math.PI);
        postDraw(applet);
    }
    
    public void setFill(float r, float g, float b){
        this.r =r;
        this.g = g;
        this.b = b;
    }
}
