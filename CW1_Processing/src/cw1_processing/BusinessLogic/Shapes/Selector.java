package cw1_processing.BusinessLogic.Shapes;

import cw1_processing.BusinessLogic.Animations.Swipe;
import cw1_processing.BusinessLogic.Animations.SwipeInterface;
import cw1_processing.BusinessLogic.Animations.Fader;
import cw1_processing.BusinessLogic.Animations.Transition;
import cw1_processing.BusinessLogic.Delay;
import cw1_processing.Constants;
import fisica.FBox;
import fisica.FCircle;
import fisica.FWorld;
import java.util.ArrayList;
import java.util.List;
import processing.core.PApplet;
import processing.core.PImage;

/**
 *
 * @author Mark
 */
public class Selector {
    
    private final PApplet parent;
    private final FWorld world;
    private final int selectorSize;
    private final int iconsOffsetFromSelector;
    private final int iconsOffsetFromEachOther;
    private final List<String> options;
    private final int instanceNumber;
    private final float rotateArrowOffsetFromScreenY;
    
    //info
    private String avatarID;
    
    //rotation
    private final int numberOfOptions;
    private final float angle;
    private final int radiusFromSelector;
    private final int iconWidth;
    private String oldAvatarID;
    
    //swipe
    private Swipe swipe;
    private SwipeInterface swipeCallBack;
    
    //arrows
    private Delay swipeArrowDelay;
    private Delay rotateArrowDelay;
    private Delay resetFaderActivity;
    private Fader rotationFader;
//    private Fader swipeFader;
    private int numberOfAvatarChanges = 0;
    private Transition arrowup;
    
    public Selector(Object parent, FWorld world, int instanceNumber, 
            int selectorSize, int iconsOffsetFromSelector, 
            int iconsOffsetFromEachOther, float swipeOffset, List<String> options) {
        //swipe functionality
        this.swipeCallBack = (SwipeInterface)parent;
        this.swipe = new Swipe(swipeOffset);
        this.oldAvatarID = "";
        
        //world and applet
        this.parent = (PApplet)parent;
        this.world = world;
        this.instanceNumber = instanceNumber;
        
        //selector dimensions
        this.selectorSize = selectorSize;
        this.iconsOffsetFromSelector = iconsOffsetFromSelector;
        this.iconsOffsetFromEachOther = iconsOffsetFromEachOther;
        
        //list of image ids
        this.options = new ArrayList<String>(options);
        //remove blank option
        this.options.remove(0);
        this.avatarID = "";
        
        //pre calculations
        this.numberOfOptions = this.options.size();
        this.angle = (float)(Math.PI/(this.numberOfOptions/2.0));
        this.radiusFromSelector = this.selectorSize+this.iconsOffsetFromSelector;
        this.iconWidth = (int)((2 * Math.PI * this.radiusFromSelector)/ 
                this.numberOfOptions)-this.iconsOffsetFromEachOther;
        this.rotateArrowOffsetFromScreenY =
                //mid center selector
                (this.selectorSize/2)+
                //offset between selector and icon
                this.iconsOffsetFromSelector+
                //icon diameters
                this.iconWidth+
                //offset from icon to arrow
                Constants.getInstance().avatarRotateArrowOffsetFromScreenY;
        
        //arrows
        this.swipeArrowDelay = new Delay();
        this.rotateArrowDelay = new Delay();
        this.resetFaderActivity = new Delay();
        
        this.rotationFader = new Fader(
                Constants.getInstance().avatarRotationMinFadeValue,
                Constants.getInstance().avatarRotationMaxFadeValue,
                Constants.getInstance().avatarRotationIncrementRate,
                Constants.getInstance().avatarRotationDelayIncrement,
                Constants.getInstance().avatarRotationFadeBothDirections,
                Constants.getInstance().avatarRotationFadeNumberOfFullFadeCycles
        );
        
//        this.swipeFader = new Fader(
//                Constants.getInstance().avatarSwipeMinFadeValue,
//                Constants.getInstance().avatarSwipeMaxFadeValue,
//                Constants.getInstance().avatarSwipeIncrementRate,
//                Constants.getInstance().avatarSwipeDelayIncrement,
//                Constants.getInstance().avatarSwipeFadeBothDirections,
//                Constants.getInstance().avatarSwipeFadeNumberOfFullFadeCycles
//        );
        
        this.arrowup = new Transition(this.parent,
                this.world,
                Constants.getInstance().arrowImagesLocation,
                Constants.getInstance().avatarSwipeArrowImage,
                Constants.getInstance().avatarSwipeTransitionOpacity,
                Constants.getInstance().avatarSwipeTransitionNumOfImages,
                Constants.getInstance().avatarSwipeTransitionDelay,
                Constants.getInstance().avatarSwipeTransitionLoopFor,
                selectorSize/2
        );
        
    }
    
    /**
     * Used to display the selector and return the id selected
     * @param screenX
     * @param screenY
     * @param selectorAngle
     * @return 
     */
    public String display(float screenX, float screenY, float selectorAngle) {
        
        //debounce the arrow actions
        reset();
        this.resetFaderActivity.start();
        
        //display the cricle at the center
        if(Constants.getInstance().avatarShowSelectorCircle){
            FCircle bola = new FCircle(this.selectorSize);
            bola.setFill(46, 204, 113);
            bola.setNoStroke();
            bola.setStatic(true);
            bola.setPosition(screenX, screenY);
            this.world.add(bola);
        }
        
        int counter = 0;
        //draw all of the icons
        for(String avatarID : this.options){
            FCircle t = new FCircle(10);
            float calcAngle = this.angle *(counter);
            float nextCalcAngle = this.angle *(counter+1);
            float x = (float)(this.radiusFromSelector * Math.cos(calcAngle));
            float y = (float)(this.radiusFromSelector * Math.sin(calcAngle));
            t.setPosition(screenX - x, screenY - y);
            PImage image = null;
            
            //if this icon is in the angle range (selected)
            if(selectorAngle >= calcAngle && selectorAngle < nextCalcAngle){
                image = this.parent.loadImage(cw1_processing.Constants.
                        getInstance().avatarImagesLocation+avatarID+".png");
                this.avatarID = avatarID;
                
                //debounce changes
                checkAvatarChange(avatarID, screenY);
                
                //swipe functionality
                if(this.swipe.checkNegativeSwipe(screenY)){
                    this.swipeCallBack.selected(this.instanceNumber);
                }
                
                //show the respective arrow
                //if the selection has not be changed twice show the rotate arrow
                //(note that placing the selector counts as one)
                if(this.numberOfAvatarChanges < 2){
                    //add the text bubble
                    addbubble(Constants.getInstance().textImagesLocation+Constants.
                            getInstance().selectorRotateTextImage+".png");
                    
                    //show the rotate arrow
                    if(this.rotateArrowDelay.action(Constants.getInstance().avatarRotateImageDelay)){
                        Image arrow = new Image(this.parent, Constants.
                                getInstance().arrowImagesLocation+Constants.
                                        getInstance().avatarRotationArrowImage+".png");
                        arrow.setPosition(screenX, screenY - this.rotateArrowOffsetFromScreenY);
                        arrow.tint(this.rotationFader.fade());
                        this.world.add(arrow);
                    }
                }else if(this.swipeArrowDelay.action(Constants.getInstance().avatarSwipeImageDelay)){
                    //show the swipe arrow
                    this.arrowup.draw(screenX, screenY);
                    
                    //add the text bubble
                    addbubble(Constants.getInstance().textImagesLocation+Constants
                            .getInstance().selectorSwipeUpTextImage+".png");
                }else{
                    //show the swip text permanently after a rotation has occured
                    addbubble(Constants.getInstance().textImagesLocation+Constants
                            .getInstance().selectorSwipeUpTextImage+".png");
                }
            }
            //if the icon is not selected just place the image of the icon respective to that angle
            else{
                image = this.parent.loadImage(cw1_processing.Constants.getInstance()
                        .avatarImagesLocation+avatarID+"_g.png");
            }
            
            //resize the image based on the calculation
            image.resize(this.iconWidth, this.iconWidth);
            t.attachImage(image);
            t.setNoStroke();
            t.setStatic(true);
            t.adjustRotation((float)(calcAngle-(Math.PI/2)));
            this.world.add(t);
            
            //debug
            //System.out.println(s + " angle:" + angle + " x:" + x + " y:" + y);
            counter++;
        }
        return this.avatarID;
    }
    
    /**
     * check for updates in the selected avatar, if the selected avatar is different from the pervious check:
     * update the avatar id with the new avatar id
     * update the screenY value
     * start the delay which will show the rotate arrow
     * This will make it possible to update the screenY value when a different avatar is selected
     * @param avatarID
     * @param screenY
     */
    private void checkAvatarChange(String avatarID, float screenY){
        if(!this.oldAvatarID.equals(avatarID)){
            this.oldAvatarID = avatarID;
            this.swipe.setOldValue(screenY);
            this.swipeArrowDelay.start();
            this.rotateArrowDelay.start();
            this.arrowup.reset();
            this.numberOfAvatarChanges++;
            //System.out.println(avatarID + " -> " + screenY);
        }
    }
    
    /**
     * if there is no activity, the numberOfAvatarChanges will reset as it would mean that the selector is not being displayed
     */
    private void reset(){
        if(this.resetFaderActivity.action(500)){
            this.numberOfAvatarChanges = 0;
        }
    }
    
    /**
     * add bubble under the character based on the action required
     * @param path 
     */
    private void addbubble(String path){
        FBox text = new FBox(0,0);
        text.setStatic(true);
        PImage img = this.parent.loadImage(path);
        text.attachImage(img);
        text.setPosition(30+(img.width/2), 100+(img.height/2));
        this.world.add(text);
    }
    
}
