package cw1_processing.BusinessLogic.Shapes;

import processing.core.PApplet;
import processing.core.PFont;

/**
 *
 * @author Mark
 */
public class Text {
    
    public static void draw(PApplet parent, float fontSize, String text, float x, float y, float r, float g, float b, float opacity){
        PFont font = parent.createFont("Arial",fontSize,true);
        parent.textFont(font);
        parent.fill(r,g,b, opacity);
        parent.text(text, x, y);
    }
}
