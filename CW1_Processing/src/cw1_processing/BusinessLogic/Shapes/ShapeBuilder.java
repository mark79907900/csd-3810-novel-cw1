package cw1_processing.BusinessLogic.Shapes;

import cw1_processing.BusinessLogic.ErrorHandling;
import cw1_processing.BusinessLogic.Sketch.Shape;
import cw1_processing.BusinessLogic.Sketch.ShapeColour;
import cw1_processing.BusinessLogic.Sketch.ShapeRelativeTo;
import cw1_processing.BusinessLogic.Sketch.ShapeRelativeToLocation;
import cw1_processing.BusinessLogic.Sketch.ShapeRelativeToLocation.ShapeRelativeToLocationX;
import cw1_processing.BusinessLogic.Sketch.ShapeRelativeToLocation.ShapeRelativeToLocationY;
import cw1_processing.BusinessLogic.Sketch.ShapeType;
import cw1_processing.BusinessLogic.Sketch.SketchLoader;
import cw1_processing.BusinessLogic.ThreadPool;
import cw1_processing.BusinessLogic.Threads.Sound;
import cw1_processing.Constants;
import cw1_processing.SharedBuffer;
import fisica.FBody;
import fisica.FBox;
import fisica.FCircle;
import fisica.FContact;
import fisica.FWorld;
import java.util.ArrayList;
import java.util.List;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * @author Mark, Lawrence & Jean
 */
public class ShapeBuilder {
    
    public static void createCircle(FWorld world, int playerId, float screenX, float screenY, int tuioSymbolId) {
        FCircle circle = new FCircle(100);
        circle.setPosition(screenX, screenY);
        circle.setNoStroke();
        circle.setBullet(true);
//        circle.setName(String.valueOf(tuioSymbolId));
        circle.setName(String.valueOf(ShapeType.CIRCLE.getShapeId()));
        ShapeColour colour = getColourFID(tuioSymbolId, playerId);
        if (colour != null) {
            circle.setFill(colour.getRgbValues().getRed(),
                    colour.getRgbValues().getGreen(),
                    colour.getRgbValues().getBlue());
        } else {
            circle.setFill(-1, -1, -1);
        }
        checkContact(world, playerId, circle);
        world.add(circle);
    }
    
    public static void createSquare(FWorld world, int playerId, float screenX, float screenY, float angle, int tuioSymbolId) {
        FBox square = new FBox(100, 100);
        square.setPosition(screenX, screenY);
        square.adjustRotation(angle);
//        square.setName(String.valueOf(tuioSymbolId));
        square.setName(String.valueOf(ShapeType.SQUARE.getShapeId()));
        ShapeColour colour = getColourFID(tuioSymbolId, playerId);
        if (colour != null) {
            square.setFill(colour.getRgbValues().getRed(),
                    colour.getRgbValues().getGreen(),
                    colour.getRgbValues().getBlue());
        } else {
            square.setFill(-1, -1, -1);
        }
        square.setNoStroke();
        square.setBullet(true);
        checkContact(world, playerId, square);
        world.add(square);
    }
    
    public static void createRectangle(FWorld world, int playerId, float screenX, float screenY, float angle, int tuioSymbolId) {
        FBox rectangle = new FBox(200, 100);
        rectangle.setPosition(screenX, screenY);
        rectangle.setNoStroke();
        rectangle.setBullet(true);
        rectangle.adjustRotation(angle);
//        rectangle.setName(String.valueOf(tuioSymbolId));
        rectangle.setName(String.valueOf(ShapeType.RECTANGLE.getShapeId()));
        ShapeColour colour = getColourFID(tuioSymbolId, playerId);
        if (colour != null) {
            rectangle.setFill(colour.getRgbValues().getRed(),
                    colour.getRgbValues().getGreen(),
                    colour.getRgbValues().getBlue());
        } else {
            rectangle.setFill(-1, -1, -1);
        }
        checkContact(world, playerId, rectangle);
        world.add(rectangle);
    }
    
    public static void createSemiCircle(PApplet parent, FWorld world, int playerId, float screenX, float screenY, float angle, int tuioSymbolId) {
        FCircle semiCircle = new FCircle(150);
        semiCircle.setPosition(screenX, screenY);
        semiCircle.setNoStroke();
        semiCircle.setBullet(true);
        semiCircle.adjustRotation(angle);
//        semiCircle.setName(String.valueOf(tuioSymbolId));
        semiCircle.setName(String.valueOf(ShapeType.SEMICIRCLE.getShapeId()));
        ShapeColour colour = getColourFID(tuioSymbolId, playerId);
        if (colour != null) {
            semiCircle.setFill(colour.getRgbValues().getRed(),
                    colour.getRgbValues().getGreen(),
                    colour.getRgbValues().getBlue());
        } else {
            semiCircle.setFill(-1, -1, -1);
        }
        checkContact(world, playerId, semiCircle);
        world.add(semiCircle);
    }
    
    public static void createTriangle(FWorld world, int playerId, float screenX, float screenY, float angle, int tuioSymbolId) {
        FTriangle triangle = new FTriangle(200);
        triangle.setPosition(screenX, screenY);
        triangle.setNoStroke();
        triangle.setBullet(true);
        triangle.adjustRotation(angle);
//        triangle.setName(String.valueOf(tuioSymbolId));
        triangle.setName(String.valueOf(ShapeType.TRIANGLE.getShapeId()));
        ShapeColour colour = getColourFID(tuioSymbolId, playerId);
        if (colour != null) {
            triangle.setFill(colour.getRgbValues().getRed(),
                    colour.getRgbValues().getGreen(),
                    colour.getRgbValues().getBlue());
        } else {
            triangle.setFill(-1, -1, -1);
        }
        checkContact(world, playerId, triangle);
        world.add(triangle);
    }
    
    //todo
    /**
     * Method to create crayon
     * @param parent use 'this' in appMain
     * @param world
     * @param colour
     * @param playerId
     * @param screenX
     * @param screenY
     * @param angle
     * @param tuioSymbolId
     */
    public static void createCrayon(PApplet parent, FWorld world, ShapeColour colour, int playerId, float screenX, float screenY, float angle, int tuioSymbolId) {
        FBox crayon = new FBox(100, 100);
        crayon.setPosition(screenX, screenY);
        crayon.setNoStroke();
        crayon.setBullet(true);
        crayon.adjustRotation(angle);
        crayon.setName(String.valueOf(tuioSymbolId));
        PImage image = parent.loadImage(colour.getImagePath());
        crayon.attachImage(image);
        checkCrayonContact(world, playerId, crayon, colour);
        world.add(crayon);
    }
    
    //todo check for colours
    private synchronized static void checkContact(FWorld mainWorld, int playerId, FBody passedBody) {
        try {
            if (mainWorld.getBodies().size() > 0) // check if there are bodies in the world
            {
                for (FBody loopBody : (ArrayList<FBody>) mainWorld.getBodies()) // loop for everybody in the world.
                {
                    if (!loopBody.getName().equals(passedBody.getName())) //To check if the object passed from the body list is not the same as the one that is being created
                    {
                        for (FContact contact : (ArrayList<FContact>) loopBody.getContacts()) // array of bodies that are in contact. These
                        {
                            if (contact.getBody2().getName().equals(passedBody.getName())) //Returns the name of the second body in contact and checks if it is equal to the body passed as a parameter.
                            {
//                        System.out.println("tuioSymbolId: " + contact.getBody2().getName() + " Name: " + contact.getBody1().getName()); // Prints out the ids of the bodies in contact
                                for (Shape shape : SketchLoader.getSketch(SharedBuffer.getInstance().getCurrentSketchId()).getShapes()) // Looping through each shape of the sketch
                                {
                                    for (ShapeRelativeTo relativeShape : shape.getRelativeTo()) {
                                        //check if the contact shapes are the shapes that are suppose to be in contact
                                        if ((shape.getShapeId() == Integer.parseInt(contact.getBody2().getName())
                                                && relativeShape.getShapeId() == Integer.parseInt(contact.getBody1().getName())) //|| (shape.getShapeId() == Integer.parseInt(contact.getBody2().getName())
                                                //&& relativeShape.getShapeId() == Integer.parseInt(contact.getBody1().getName()))
                                                ) {
                                            
                                            //System.out.println("id :" + relativeShape.getShapeId() + " x: " + (passedBody.getX() - loopBody.getX()));
                                            //System.out.println("y: " + (passedBody.getY() - loopBody.getY()));
                                            //check that the shapes in contact are at the right position
                                            boolean horizontalCheck = checkHorziontal(passedBody, loopBody, relativeShape.getReferenceLocation());
                                            boolean verticalCheck = checkVertical(passedBody, loopBody, relativeShape.getReferenceLocation());
                                            
                                            //check the parent's angle
                                            //System.out.println("shape: " + contact.getBody1().getName() + " angle: " + contact.getBody1().getRotation() + " compare:" +shape.getPlottingLocation().getAngle());
                                            boolean angleShapeCheck = checkAngle(contact.getBody1(), relativeShape.getReferenceLocation().getAngle());
                                            //check the child's angle
                                            //System.out.println("relative: " + contact.getBody2().getName() + " angle: " + contact.getBody2().getRotation() + " compare:" +relativeShape.getReferenceLocation().getAngle());
                                            boolean angleRelativeCheck = checkAngle(contact.getBody2(), shape.getPlottingLocation().getAngle());
                                            
                                            //check that the parent's color is correct
                                            boolean colourShapeCheck = checkColour(contact.getBody1(), playerId);
                                            //boolean colourShapeCheck = true;
                                            
                                            //check that the childs color is correct
                                            boolean colourRelativeCheck = checkColour(contact.getBody2(), playerId);
                                            //boolean colourRelativeCheck = true;
                                            
                                            //System.out.println("horizontal:" + horizontalCheck + " vertical:" + verticalCheck);
                                            if (horizontalCheck && verticalCheck && angleShapeCheck && angleRelativeCheck && colourShapeCheck && colourRelativeCheck) {
                                                relativeShape.setPlayerInContact(playerId);
                                                //System.out.println("WIN");
                                            } else {
                                                relativeShape.removePlayerContact(playerId);
                                            }
//                                            ThreadPool.pushThread(new Sound(Sound.play.WOODENBLOCK));
                                            //System.out.println("ref: " + contact.getBody2().getName() + " body: " + contact.getBody1().getName());
                                        }else if ((shape.getShapeId() == Integer.parseInt(contact.getBody1().getName())
                                                && relativeShape.getShapeId() == Integer.parseInt(contact.getBody2().getName())) //|| (shape.getShapeId() == Integer.parseInt(contact.getBody2().getName())
                                                //&& relativeShape.getShapeId() == Integer.parseInt(contact.getBody1().getName()))
                                                ) {
                                            //System.out.println("id :" + relativeShape.getShapeId() + " x: " + (passedBody.getX() - loopBody.getX()));
                                            //System.out.println("y: " + (passedBody.getY() - loopBody.getY()));
                                            //check that the shapes in contact are at the right position
                                            boolean horizontalCheck = checkHorziontal(loopBody, passedBody, relativeShape.getReferenceLocation());
                                            boolean verticalCheck = checkVertical(loopBody, passedBody, relativeShape.getReferenceLocation());
                                            
                                            //check the parent's angle
                                            //System.out.println("shape: " + contact.getBody1().getName() + " angle: " + contact.getBody1().getRotation() + " compare:" +shape.getPlottingLocation().getAngle());
                                            boolean angleShapeCheck = checkAngle(contact.getBody2(), relativeShape.getReferenceLocation().getAngle());
                                            //check the child's angle
                                            //System.out.println("relative: " + contact.getBody2().getName() + " angle: " + contact.getBody2().getRotation() + " compare:" +relativeShape.getReferenceLocation().getAngle());
                                            boolean angleRelativeCheck = checkAngle(contact.getBody1(), shape.getPlottingLocation().getAngle());
                                            
                                            //check that the parent's color is correct
                                            boolean colourShapeCheck = checkColour(contact.getBody1(), playerId);
                                            //boolean colourShapeCheck = true;
                                            
                                            //check that the childs color is correct
                                            boolean colourRelativeCheck = checkColour(contact.getBody2(), playerId);
                                            //boolean colourRelativeCheck = true;
                                            
                                            //System.out.println("horizontal:" + horizontalCheck + " vertical:" + verticalCheck);
                                            if (horizontalCheck && verticalCheck && angleShapeCheck && angleRelativeCheck && colourShapeCheck && colourRelativeCheck) {
                                                relativeShape.setPlayerInContact(playerId);
                                                //System.out.println("WIN");
                                            } else {
                                                relativeShape.removePlayerContact(playerId);
                                            }
//                                            ThreadPool.pushThread(new Sound(Sound.play.WOODENBLOCK));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ErrorHandling.OutputException(ex);
        }
    }
    
    /**
     * This method should be used to check if a crayon has made contact with a
     * shape, if it did it should update the list
     *
     * @param mainWorld
     * @param playerId
     * @param crayon
     */
    private static void checkCrayonContact(FWorld mainWorld, int playerId, FBody crayon, ShapeColour crayonColour) {
        try {
            if (mainWorld.getBodies().size() > 0) // check if there are bodies in the world
            {
                for (FBody loopBody : (ArrayList<FBody>) mainWorld.getBodies()) // loop for everybody in the world.
                {
                    if (loopBody.getName().equals(crayon.getName())) //To check if the object passed from the body list is not the same as the one that is being created
                    {
                        for (FContact contact : (ArrayList<FContact>) loopBody.getContacts()) // array of bodies that are in contact
                        {
                            if (contact.getBody1().getName().equals(crayon.getName())) //Returns the name of the second body in contact and checks if it is equal to the body passed as a parameter.
                            {
//                        System.out.println("tuioSymbolId: " + contact.getBody2().getName() + " Name: " + contact.getBody1().getName()); // Prints out the ids of the bodies in contact
                                //loop the sketch to get the colour id for the shape in contact
                                for (Shape shape : SketchLoader.getSketch(SharedBuffer.getInstance().getCurrentSketchId()).getShapes()) // Looping through each shape of the sketch
                                {
                                    //implementation
                                    //check that the shape is the same shape that is in contact
                                    if (shape.getShapeId() == Integer.parseInt(contact.getBody2().getName())) {
                                        //contact.getBody2() huwa == al crayon
                                        //shape uwa is shape li qed ticekja mijaw mil li sketch
                                        //contact.getBody1() andu jkun == al shape. issa ftakar li shape uwa dak li gej min sketch so mandux cordinates u body1 mandux colour id
                                        
                                        //gibu il colour id mix shape eg: shape.getColourID()
                                        //gibu is shape id shape.getShapeId() li ukoll il fidusial id ax == contact.getBody1() li uwa ix shape li hemm hemm bara
                                        //andkom playerId ga adej
                                        //hekk andkom [fiducial id/shape id, player id, u current color id], uzaw din id data biex ticekjaw jekk shape id andux ga entry fil list jekk le zidu entry jekk iwa updetjawa.
                                        int shapeFiducialId = shape.getShapeType().getFiducialId();
//                                        int colourId = Integer.parseInt(contact.getBody1().getName());
                                        boolean updated = false;
                                        for (ShapePlayerColour spc : SharedBuffer.getInstance().getShapePlayerColours()) {
                                            if (spc.getShapeFiducialId() == shapeFiducialId && spc.getPlayerID() == playerId) {
                                                updated = true;
                                                spc.setColour(crayonColour);
                                                ThreadPool.pushThread(new Sound(Sound.play.WOODENBLOCK));
                                            }
                                        }
                                        if (!updated) {
                                            SharedBuffer.getInstance().addShapePlayerColour(new ShapePlayerColour(shape.getShapeType().getShapeId(),shapeFiducialId, playerId, crayonColour));
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ErrorHandling.OutputException(ex);
        }
    }
    
//    private static boolean skipCrayons(){
//
//    }
    
    /**
     * check if the object is placed horizontally correct
     *
     * @param passedBody
     * @param loopBody
     * @param shapeRelativeToLocation
     * @return
     */
    private static boolean checkHorziontal(FBody passedBody, FBody loopBody, ShapeRelativeToLocation shapeRelativeToLocation) {
        if (passedBody.getX() - loopBody.getX() > 0) {
            //System.out.println("right");
            return shapeRelativeToLocation.getX() == ShapeRelativeToLocationX.right || shapeRelativeToLocation.getX() == ShapeRelativeToLocationX.ignore;
        } else {
            //System.out.println("left");
            return shapeRelativeToLocation.getX() == ShapeRelativeToLocationX.left || shapeRelativeToLocation.getX() == ShapeRelativeToLocationX.ignore;
        }
    }
    
    /**
     * check if the object is placed vertically correct
     *
     * @param passedBody
     * @param loopBody
     * @param shapeRelativeToLocation
     * @return
     */
    private static boolean checkVertical(FBody passedBody, FBody loopBody, ShapeRelativeToLocation shapeRelativeToLocation) {
        if (passedBody.getY() - loopBody.getY() > 0) {
            //System.out.println("bottom");
            return shapeRelativeToLocation.getY() == ShapeRelativeToLocationY.bottom || shapeRelativeToLocation.getY() == ShapeRelativeToLocationY.ignore;
        } else {
            //System.out.println("top");
            return shapeRelativeToLocation.getY() == ShapeRelativeToLocationY.top || shapeRelativeToLocation.getY() == ShapeRelativeToLocationY.ignore;
        }
    }
    
    /**
     * check if the angle of the object is correct
     *
     * @param body
     * @param angle
     * @return
     */
    public static boolean checkAngle(FBody body, float angle) {
        float calcAngleOffset = (float) Math.toRadians(Constants.getInstance().angleOffset);
        float rotation = body.getRotation();
        //the problem is that if the angle gets below 1deg below 0deg, it will turn to 359 rather -1 deg
        //first part will cater from 0 to angle + offset
        //second part will cater for 0 to 360-(angle + offset)
        if ( //first part 0 to (angle + offset)
                (rotation >= angle && rotation <= angle + calcAngleOffset)
                || //second part 0 to (360 - (angle +offset)) simulating when the angle gets below 0 deg
                (rotation <= (Math.PI * 2) && rotation >= ((Math.PI * 2) - (angle + calcAngleOffset)))) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * check if the colour of the object is correct
     *
     * @param body
     * @param playerID
     * @return
     */
    public static boolean checkColour(FBody body, int playerID) {
        
        int passedShapeId = Integer.parseInt(body.getName());
        
        //loop in the sketch
        for (Shape shape : SketchLoader.getSketch(SharedBuffer.getInstance().getCurrentSketchId()).getShapes()) {
            //find the shape we have passed
            if (shape.getShapeId() == passedShapeId) {
                if(shape.getColour() == getColourShapeID(passedShapeId, playerID)){
                    return true;
                }
                //getcolourID uwa dak l id li ser tamlu gdid
                //listOfShapes ija dik il lista li ha tamlu biex izomu is shapes u kuluri
                //getcolorID uwa method li tridu tamlu lil playerShapeColorList li jisearchja gol lista al both (playerID u passedShapeId) u jekk isib entry, igib il colorid. jekk le igib -1
                //if(shape.getcolourID() == playerShapeColorList.getcolorID(playerID, passedShapeId){
                //return true
                //}
            }
        }
        return false;
    }
    
    /**
     * Check if the player has won or not
     *
     * @param playerID
     * @return
     */
    public synchronized static boolean checkWin(int playerID) {
        for (Shape shape : SketchLoader.getSketch(SharedBuffer.getInstance().getCurrentSketchId()).getShapes()) { //Looping through the shapes of the specific sketch
            for (ShapeRelativeTo relativeShape : shape.getRelativeTo()) { //Looping through the shapes relative to the current shape
                //the 0 is important as an empty array in the sketcj.json will get set to shape id 0
                if (relativeShape.getShapeId() != shape.getShapeId() && relativeShape.getShapeId() != 0) { //todo - Ask mark whether the relative shape id cannot be equal to 0 or not equal
                    //this should be split as the relativeShapes that are (relativeShape.getShapeId() == shape.getShapeId()) do not have a player contact list
                    if (!relativeShape.isPlayerInContact(playerID)) {
                        //System.out.println(relativeShape.getShapeId() + ":" + relativeShape.isPlayerInContact(playerID));
                        return false;
                    }
                }
            }
        }
        //call win sound
        return true;
    }
    
    private static ShapeColour getColourShapeID(int shapeId, int playerID) {
        List<ShapePlayerColour> spcList = SharedBuffer.getInstance().getShapePlayerColours();
        if(spcList.size() > 0){
            for (ShapePlayerColour spc : spcList) {
                if (spc.getShapeId() == shapeId && spc.getPlayerID() == playerID) {
                    return spc.getColour();
                }
            }
        }
        return null;
    }
    
    private static ShapeColour getColourFID(int shapeId, int playerID) {
        List<ShapePlayerColour> spcList = SharedBuffer.getInstance().getShapePlayerColours();
        if(spcList.size() > 0){
            for (ShapePlayerColour spc : spcList) {
                if (spc.getShapeFiducialId()== shapeId && spc.getPlayerID() == playerID) {
                    return spc.getColour();
                }
            }
        }
        return null;
    }
}
