package cw1_processing.BusinessLogic.Shapes;

import cw1_processing.BusinessLogic.Sketch.ShapeColour;

/**
 *
 * @author lawrencesaliba
 */
public class ShapePlayerColour {
    private int shapeId;
    private int shapeFiducialId;
    private int playerID;
    private ShapeColour colour;

    public ShapePlayerColour(int shapeId, int shapeFiducialId, int playerID, ShapeColour colour) {
        this.shapeId = shapeId;
        this.shapeFiducialId = shapeFiducialId;
        this.playerID = playerID;
        this.colour = colour;
    }

    public int getShapeId() {
        return shapeId;
    }

    public void setShapeId(int shapeId) {
        this.shapeId = shapeId;
    }

    public int getShapeFiducialId() {
        return shapeFiducialId;
    }

    public void setShapeFiducialId(int shapeFiducialId) {
        this.shapeFiducialId = shapeFiducialId;
    }

    public int getPlayerID() {
        return playerID;
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }

    public ShapeColour getColour() {
        return colour;
    }

    public void setColour(ShapeColour colour) {
        this.colour = colour;
    }

   
}
