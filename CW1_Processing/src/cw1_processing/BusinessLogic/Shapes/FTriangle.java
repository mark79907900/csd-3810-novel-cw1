package cw1_processing.BusinessLogic.Shapes;

import fisica.FPoly;

/**
 *
 * @author Mark
 */
public class FTriangle extends FPoly{
    
    /**
     * create a right angle triangle
     * @param h height
     * @param b base
     */
    public FTriangle(float h, float b) {
        setStatic(true);
        setBullet(true);
        setNoStroke();
        vertex(getX()-(b/2),getY()-(b/2));
        vertex(getX()+(b/2), getY()-(b/2));
        vertex(getX()-(b/2), getY()+(b/2));
    }
    
    /**
     * method used to create equilateral triangle
     * @param b base
     */
    public FTriangle(float b) {
        setStatic(true);
        setBullet(true);
        vertex(getX()-(b/2),getY()+((b/2)*(float)Math.tan(Math.PI/6)));
        vertex(getX()+(b/2), getY()+((b/2)*(float)Math.tan(Math.PI/6)));
        vertex(getX(), -(float)((b*Math.sin(Math.PI/3))/2));
    }
    
}
