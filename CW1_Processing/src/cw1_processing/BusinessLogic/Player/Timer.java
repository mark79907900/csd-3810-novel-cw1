package cw1_processing.BusinessLogic.Player;

import cw1_processing.BusinessLogic.Animations.Fader;
import cw1_processing.BusinessLogic.Shapes.Text;
import cw1_processing.Constants;
import cw1_processing.UpdateStaticWorld;
import fisica.FBox;
import fisica.FWorld;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import processing.core.PApplet;
import processing.core.PImage;

/**
 *
 * @author Mark
 */
public class Timer {
    private long startGame;
    private final long duration;
    private final long countDownEffect;
    private String timerPath;
    private PImage image;
    private boolean countDownEffectFlag;
    private final UpdateStaticWorld updateStaticWorld;
    private final NumberFormat formatter;
    private final Fader fader;
    
    
    public Timer(UpdateStaticWorld callback, long duration, long countDownEffect) {
        this.duration = duration;
        this.startGame = System.currentTimeMillis();
        this.countDownEffect = countDownEffect;
        this.countDownEffectFlag = false;
        this.updateStaticWorld = callback;
        this.formatter = new DecimalFormat("00");
        this.fader = new Fader(
                Constants.getInstance().timerMinFadeValue,
                Constants.getInstance().timerMaxFadeValue,
                Constants.getInstance().timerFadeIncrementRate,
                Constants.getInstance().timerFadeDelayIncrement,
                Constants.getInstance().timerFadeBothDirections,
                Constants.getInstance().timerFadeNumberOfFullFadeCycles
        );
    }
    
    public synchronized void diaplayStatic(PApplet parent, FWorld world, float screenWidth){
        if(this.countDownEffectFlag){
            this.timerPath = String.format("%s%s%s", Constants.getInstance().otherImagesLocation, Constants.getInstance().timerExpiringImage, ".png");
        }else{
            this.timerPath = String.format("%s%s%s", Constants.getInstance().otherImagesLocation, Constants.getInstance().timerImage, ".png");
        }
        this.image = parent.loadImage(this.timerPath);
        this.image.resize(100, 100);
        FBox avatar = new FBox(0, 0);
        avatar.setPosition(screenWidth-60, 55);
        avatar.attachImage(this.image);
        world.add(avatar);
    }
    
    public synchronized boolean displayDynamic(PApplet parent, FWorld world, float screenWidth){
        long millis = this.duration - (System.currentTimeMillis() - this.startGame);
        long seconds = (millis/1000)%60;
        long minutes = ((millis-seconds)/1000)/60;
        String strSeconds = this.formatter.format(seconds);
        String strMinutes = this.formatter.format(minutes);
        
        if(millis > this.countDownEffect){
            Text.draw(
                    parent,
                    30,
                    strMinutes+":"+strSeconds,
                    screenWidth-98, 64,
                    46, 146, 25,
                    255
            );
        }else{
            
            if(!this.countDownEffectFlag){
                this.updateStaticWorld.update();
                this.countDownEffectFlag = true;
            }
            
            Text.draw(
                    parent,
                    30,
                    strMinutes+":"+strSeconds,
                    screenWidth-98, 64,
                    146, 0, 0,
                    this.fader.fade()
            );
        }
        return millis < 0;
    }
    
    
    public void startGame(){
        this.startGame = System.currentTimeMillis();
    }
}
