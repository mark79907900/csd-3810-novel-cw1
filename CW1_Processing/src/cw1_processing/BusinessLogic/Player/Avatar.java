/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package cw1_processing.BusinessLogic.Player;

import fisica.FBox;
import fisica.FWorld;
import processing.core.PApplet;
import processing.core.PImage;

/**
 *
 * @author Mark
 */
public class Avatar{

    private String avatarName;
    private String avatarPath;
    private PImage image;
    
    public Avatar(String avatarName) {
        this.avatarName = avatarName;
        this.avatarPath = String.format("%s%s%s", cw1_processing.Constants
                .getInstance().avatarImagesLocation, avatarName, ".png");
    }
    
    public void display(PApplet parent, FWorld world){
        //https://processing.org/tutorials/eclipse/
        this.image = parent.loadImage(this.avatarPath);
        this.image.resize(100, 100);
        FBox avatar = new FBox(0, 0);
        avatar.setPosition(55, 55);
        avatar.attachImage(this.image);
        world.add(avatar);
    }
    
    
}
