/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package cw1_processing.BusinessLogic.Player;

import cw1_processing.GameState;

/**
 *
 * @author Mark
 */
public class Player {
    private String avatarID;
    private GameState playerState;
    private boolean updateStaticWorld;
    private ResetGame resetGame;
    
    public Player() {
        this.playerState = GameState.AvatarSelection;
        this.updateStaticWorld = false;
        this.resetGame = ResetGame.NO_RESET;
    }

    public String getAvatarID() {
        return avatarID;
    }

    public void setAvatarID(String avatarID) {
        this.avatarID = avatarID;
    }

    public GameState getPlayerState() {
        return playerState;
    }

    public void setWizzardState(GameState wizzardState) {
        this.playerState = wizzardState;
    }
    
    public void updateStaticWorld(boolean update){
        this.updateStaticWorld = update;
    }
    
    public boolean isUpdateStaticWorld(){
        return this.updateStaticWorld;
    }

    public ResetGame getResetGame() {
        return resetGame;
    }

    public void setResetGame(ResetGame resetGame) {
        this.resetGame = resetGame;
    }
}
