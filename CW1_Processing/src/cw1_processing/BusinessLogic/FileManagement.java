package cw1_processing.BusinessLogic;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 *
 * @author jeangatt
 */
public class FileManagement {
    
    public static String readFile(String fileName){
        StringBuilder sb = new StringBuilder();
        try(FileReader fr = new FileReader(fileName)){
            try(BufferedReader br = new BufferedReader(fr)){
                String line;
                while((line = br.readLine())!= null){
                    sb.append(line);
                    line = null;
                }
            }
        }catch(Exception ex){
            ErrorHandling.OutputException(ex);
        }
        return sb.toString();
    }
}
