/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package cw1_processing.BusinessLogic;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This class is responsible with the config file 
 * @author Mark
 */
public class Config {
    private static volatile Config config = null;
    private static Properties properties;
    
    public static Config init(String configDestination){
        if(config == null){
            config = new Config(configDestination);
        }
        return config;
    }
    
    /**
     * Get the Config from the passed destination and load them in the properties object
     * @param configDestination 
     */
    private Config(String configDestination){
        properties = new Properties();
        
        try(InputStream inputStream = new FileInputStream(configDestination)){
            properties.load(inputStream);
            inputStream.close();
        }catch(FileNotFoundException ex){
            ErrorHandling.OutputException(ex);
        }catch(Exception ex){
            ErrorHandling.OutputException(ex);
        }
    }
    
    /**
     * Method used to get the value for the passed property name
     * @param property
     * @return 
     */
    public static String getProperty(String property){
        return properties.getProperty(property);
    }
}
