package cw1_processing.BusinessLogic.Sketch;

/**
 *
 * @author jeangatt
 */
public class Shape {
    private final int shapeId;
    private final ShapeType shapeType;
    private final ShapeSize size;
    private final ShapeColour colour;
    private final ShapeLocation plottingLocation;
    private final ShapeLocation referenceLocation;
    private final ShapeRelativeTo[] relativeTo;

    public Shape(int shapeId, String shapeType, ShapeSize size, ShapeColour 
            colour, ShapeLocation plottingLocation, ShapeLocation 
                    referenceLocation, ShapeRelativeTo[] relativeTo) {
        this.shapeId = shapeId;
        this.shapeType = ShapeType.valueOf(shapeType.toUpperCase());
        this.size = size;
        this.colour = colour;
        this.plottingLocation = plottingLocation;
        this.referenceLocation = referenceLocation;
        this.relativeTo = relativeTo;
    }

    public int getShapeId() {
        return shapeId;
    }

    public ShapeType getShapeType() {
        return shapeType;
    }
    
    public ShapeSize getSize() {
        return size;
    }

    public ShapeColour getColour() {
        return colour;
    }

    public ShapeLocation getPlottingLocation() {
        return plottingLocation;
    }

    public ShapeLocation getReferenceLocation() {
        return referenceLocation;
    }

    public ShapeRelativeTo[] getRelativeTo() {
        return relativeTo;
    }

    
    
}
