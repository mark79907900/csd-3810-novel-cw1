package cw1_processing.BusinessLogic.Sketch;

/**
 *
 * @author jeangatt
 */
public class ShapeRelativeToLocation {

    public enum ShapeRelativeToLocationX {

        left,
        right,
        ignore
    }
    
    public enum ShapeRelativeToLocationY {

        top,
        bottom,
        ignore
    }

    private final ShapeRelativeToLocationX x;
    private final ShapeRelativeToLocationY y;
    private final float angle;

    public ShapeRelativeToLocation(ShapeRelativeToLocationX x, ShapeRelativeToLocationY y, float angle) {
        this.x = x;
        this.y = y;
        this.angle = (float)Math.toRadians(angle);
    }

    public ShapeRelativeToLocationX getX() {
        return x;
    }

    public ShapeRelativeToLocationY getY() {
        return y;
    }

    public float getAngle() {
        return (float)Math.toRadians(angle);
    }
}
