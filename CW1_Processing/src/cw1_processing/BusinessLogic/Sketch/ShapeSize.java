package cw1_processing.BusinessLogic.Sketch;

/**
 *
 * @author jeangatt
 */
public class ShapeSize {
    private float base;
    private float height;
    private float radius;

    public ShapeSize(float base, float height) {
        this.base = base;
        this.height = height;
    }

    public ShapeSize(float radius) {
        this.radius = radius;
    }

    public float getBase() {
        return base;
    }

    public float getHeight() {
        return height;
    }

    public float getRadius() {
        return radius;
    }
}
