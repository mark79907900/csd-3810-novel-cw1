package cw1_processing.BusinessLogic.Sketch;

import java.util.ArrayList;

/**
 *
 * @author jeangatt
 */
public class ShapeRelativeTo {
    private final int shapeId;
    private final ShapeType shapeType;
    private final ShapeColour colour;
    private final ShapeRelativeToLocation referenceLocation;
    private ArrayList<Integer> playersInContact = new ArrayList<>();

    public ShapeRelativeTo(int shapeId, ShapeType shapeType, ShapeColour colour, ShapeRelativeToLocation referenceLocation, ArrayList<Integer> playersInContact) {
        this.shapeId = shapeId;
        this.shapeType = shapeType;
        this.colour = colour;
        this.referenceLocation = referenceLocation;
        this.playersInContact = playersInContact;
    }
    
    public int getShapeId() {
        return shapeId;
    }
    
    public ShapeType getShapeType() {
        return shapeType;
    }

    public ShapeColour getColour() {
        return colour;
    }
    
    public ShapeRelativeToLocation getReferenceLocation() {
        return referenceLocation;
    }
    
    public boolean isPlayerInContact(int playerID) {
        return this.playersInContact.contains(playerID);
    }
    
    public void clearPlayersInContact(){
        this.playersInContact.clear();
    }
    
    public void removePlayerContact(int playerID){
        if(this.playersInContact != null){
            int index = this.playersInContact.indexOf(playerID);
            if(index > 0){
                this.playersInContact.remove(index);
            }
        }
        
    }
    
    public void setPlayerInContact(int playerID) {
        this.playersInContact.add(playerID);
    }
}
