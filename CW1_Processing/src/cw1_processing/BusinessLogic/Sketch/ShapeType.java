package cw1_processing.BusinessLogic.Sketch;

import cw1_processing.BusinessLogic.ErrorHandling;

/**
 *
 * @author jeangatt
 */
public enum ShapeType {
    TRIANGLE(1,10),
    SQUARE(2,11),
    RECTANGLE(3,12),
    CIRCLE(4,13),
    SEMICIRCLE(5,14);
    
    private final int shapeId;
    private final int fiducialId;
    
    private ShapeType(int shapeId, int fiducialId) {
        this.shapeId = shapeId;
        this.fiducialId = fiducialId;
    }
    
    public int getShapeId() {
        return shapeId;
    }
    
    public int getFiducialId() {
        return fiducialId;
    }
    
    public static ShapeType retrieveShapeTypeViaFiducialId(int fiducialID){
        try{
            for(ShapeType shape: ShapeType.values()){
                if(fiducialID == shape.getFiducialId()){
                    return shape;
                }
            }
        }
        catch(Exception ex){
            ErrorHandling.OutputException(ex);
        }
        return null;
        
    }
//
//    public static int retrieveFiducialIdViaShapeId(int shapeID){
//        for(ShapeType shape: ShapeType.values()){
//            if(shapeID == shape.getShapeId()){
//                return shape.getFiducialId();
//            }
//        }
//        return -1;
//    }
}
