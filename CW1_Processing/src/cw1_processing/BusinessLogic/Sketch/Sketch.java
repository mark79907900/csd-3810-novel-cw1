package cw1_processing.BusinessLogic.Sketch;

import cw1_processing.BusinessLogic.Shapes.FSemiCircle;
import cw1_processing.BusinessLogic.Shapes.FTriangle;
import fisica.FBody;
import fisica.FBox;
import fisica.FCircle;
import fisica.FWorld;
import processing.core.PApplet;

/**
 *
 * @author jeangatt
 */
public class Sketch {
    
    private final int sketchId;
    private final String sketchName;
    private final Shape[] shapes;
    
    public Sketch(int sketchId, String sketchName, Shape[] shapes) {
        this.sketchId = sketchId;
        this.sketchName = sketchName;
        this.shapes = shapes;
    }
    
    public int getSketchId() {
        return sketchId;
    }
    
    public String getSketchName() {
        return sketchName;
    }
    
    public Shape[] getShapes() {
        return shapes;
    }
    
    public void draw(PApplet parent, FWorld world, int screenWidth, int screenHeight){
        //todo impelement
        for(Shape shape :this.shapes){
            FBody body = null;
            switch(shape.getShapeType()){
                case CIRCLE:{
                    body = new FCircle(shape.getSize().getRadius());
                    break;
                }
                case SEMICIRCLE:{
                    body = new FCircle(shape.getSize().getRadius());
                    break;
                }
                case RECTANGLE:
                case SQUARE:{
                    body = new FBox(shape.getSize().getBase(), shape.getSize().getHeight());
                    body.adjustRotation(shape.getPlottingLocation().getAngle());
                    break;
                }
                case TRIANGLE:{
                    body = new FTriangle(shape.getSize().getBase());
                    body.adjustRotation(shape.getPlottingLocation().getAngle());
                    break;
                }
            }
            
            //todo - check
            float x = (screenWidth/2)+shape.getPlottingLocation().getX();
            float y = (screenHeight/4)+shape.getPlottingLocation().getY();
            body.setPosition(x, y);
            body.setFill(shape.getColour().getRgbValues().getRed(), 
                    shape.getColour().getRgbValues().getGreen(), 
                    shape.getColour().getRgbValues().getBlue());
            world.add(body);
        }
    }
}
