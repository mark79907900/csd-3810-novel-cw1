package cw1_processing.BusinessLogic.Sketch;

import java.util.HashSet;

/**
 *
 * @author jeangatt
 */
public class SketchValidator {

    public static void validate(Sketch[] sketches) {
        boolean checkFailed;
        checkFailed = validateSketchId(sketches);
        checkFailed = validateShapesPerSketch(sketches);
        if (checkFailed == true) {
            System.out.println("Please fix the errors in the sketches.json file "
                    + "before continuing!");
            System.exit(0);
        }
    }

    private static boolean validateSketchId(Sketch[] sketches) {
        HashSet<Integer> ids = new HashSet<>();
        for (Sketch sketch : sketches) {
            if (ids.contains(sketch.getSketchId())) {
                System.out.println(String.format("The same sketch ID -> %s, is used "
                        + "more than once. Please fix this before continuing!", sketch.getSketchId()));
                return true;
            } else {
                ids.add(sketch.getSketchId());
            }
        }
        return false;
    }

    private static boolean validateShapesPerSketch(Sketch[] sketches) {
        for (Sketch sketch : sketches) {
            for (Shape shape : sketch.getShapes()) {
                ShapeSize size = shape.getSize();
                if (shape.getShapeType() == ShapeType.CIRCLE || shape.getShapeType() == ShapeType.SEMICIRCLE) {
                    if (size.getRadius()<= 0) {
                        System.out.println(String.format("The circle found for "
                                + "the following sketch -> %s, has negative or no radius!", sketch.getSketchName()));
                        return true;
                    }
                } else {
                    if (size.getBase() <= 0 || size.getHeight() <= 0) {
                        System.out.println(String.format("The %s found for "
                                + "the following sketch -> %s, has negative or no base and height!",
                                shape.getShapeType().toString(), sketch.getSketchName()));
                        return true;
                    }
                }
                if(shape.getShapeId() != shape.getShapeType().getShapeId()){
                    System.out.println(String.format("The shape id passed %d, "
                            + "does not match the shape id of this shape it is: %s.", 
                            shape.getShapeId(), shape.getShapeType().name()));
                    return true;
                }
            }
        }
        return false;
    }
}
