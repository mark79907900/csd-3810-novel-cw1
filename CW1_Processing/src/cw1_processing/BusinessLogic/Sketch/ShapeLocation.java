/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw1_processing.BusinessLogic.Sketch;

/**
 *
 * @author jeangatt
 */
public class ShapeLocation {
    private final float x;
    private final float y;
    private final float angle;

    public ShapeLocation(float x, float y, float angle) {
        this.x = x;
        this.y = y;
        this.angle = (float)Math.toRadians(angle);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getAngle() {
        return (float)Math.toRadians(angle);
    }
}
