package cw1_processing.BusinessLogic.Sketch;

/**
 *
 * @author jeangatt
 */
public class RGBColour {
    private final float red;
    private final float green;
    private final float blue;

    public RGBColour(float r, float g, float b) {
        this.red = r;
        this.green = g;
        this.blue = b;
    }

    public float getRed() {
        return red;
    }

    public float getGreen() {
        return green;
    }

    public float getBlue() {
        return blue;
    }
}
