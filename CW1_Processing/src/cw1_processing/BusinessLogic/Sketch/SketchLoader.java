package cw1_processing.BusinessLogic.Sketch;

import com.google.gson.Gson;
import cw1_processing.BusinessLogic.FileManagement;
import cw1_processing.Constants;

/**
 *
 * @author jeangatt
 */
public class SketchLoader {
    
    private static Sketch[] sketches;
    private static SketchLoader instance = null;
    private static Gson gson;
    
    private SketchLoader() {
        gson = new Gson();
    }
    
    public static void init() {
        if (SketchLoader.instance == null) {
            SketchLoader.instance = new SketchLoader();
        }
        SketchLoader.instance.loadSketches();
    }
    
    public static void reset() {
        SketchLoader.instance = null;
        SketchLoader.init();
    }
    
    private void loadSketches() {
        //Going to read the skecthes file and then add sketches to the list from the json
        String sketchesJsonFile = FileManagement.
                readFile(Constants.getInstance().sketchesInfo);
        sketches = gson.fromJson(sketchesJsonFile, Sketch[].class);
        SketchValidator.validate(sketches);
        System.out.println(String.format("A total of %d sketch/es "
                + "have been successfully loaded!", sketches.length));
    }
    
//    public static Sketch getSketch(int id) {
//        System.out.println(sketches[id].getSketchName());
//        return sketches[id];
//    }
    
    public static Sketch getSketch(int id) {
        //System.out.println(sketches[id].getSketchName());
        return sketches[id-1];
    }
    
    public static Sketch[] getSketches() {
        return sketches;
    }
}
