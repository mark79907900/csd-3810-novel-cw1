package cw1_processing.BusinessLogic.Sketch;

/**
 *
 * @author jeangatt
 */
public enum ShapeColour {

    BLUE(20, null, new RGBColour(52, 152, 219)),
    GREEN(21, null, new RGBColour(46, 204, 113)),
    RED(22, null, new RGBColour(231, 76, 60)),
    YELLOW(23, null, new RGBColour(241, 196, 15)),
    ORANGE(24, null, new RGBColour(243, 156, 18));

    private final int fiducialId;
    private String imagePath;
    private final RGBColour rgbValues;

    private ShapeColour(int fiducialID, String imagePath, RGBColour rgbValues) {
        this.fiducialId = fiducialID;
        this.imagePath = imagePath;
        this.rgbValues = rgbValues;
    }

    public int getFiducialId() {
        return fiducialId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public RGBColour getRgbValues() {
        return rgbValues;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public static ShapeColour retrieveShapeColourViaFiducialId(int fiducialID) {
        for (ShapeColour colour : ShapeColour.values()) {
            if (fiducialID == colour.getFiducialId()) {
                return colour;
            }
        }
        return null;
    }
}
