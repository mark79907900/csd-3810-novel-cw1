package cw1_processing.BusinessLogic.Animations;

/**
 *
 * @author Mark
 */
public class Swipe {
    private final float swipeAmount;
    private float oldValue;
    
    public Swipe(float swipeAmount){
        this.swipeAmount = swipeAmount;
        this.oldValue = 0;
    }
    
    public void setOldValue(float oldValue){
        this.oldValue = oldValue;
    }
    
    /**
     * swipe where the value is decreasing such as upwards swipe or swipe to the left
     * @param newValue new value
     * @return 
     */
    public boolean checkNegativeSwipe(float newValue){
        return (this.oldValue - newValue >= this.swipeAmount);
    }
    
    /**
     * swipe where the value is increasing such as downwards swipe or swipe to the right
     * @param newValue new value
     * @return 
     */
    public boolean checkPositiveSwipe(float newValue){
        return (newValue - this.oldValue >= this.swipeAmount);
    }
}
