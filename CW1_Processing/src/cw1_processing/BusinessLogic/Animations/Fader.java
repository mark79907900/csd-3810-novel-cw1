/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package cw1_processing.BusinessLogic.Animations;

import cw1_processing.BusinessLogic.Delay;

/**
 *
 * @author Mark
 */
public class Fader {
    private final float minValue;
    private final float maxValue;
    private final float incrementRate;
    private final long incDelayValue;
    private final boolean switchDirection;
    private boolean direction;
    private float fadeValue;
    private final Delay incDelay;
    private final long numberOfFullFadeCycles;
    private long fullFadeCyclesCounter;
    private final Delay activityDelay;
    
    public Fader(float minValue, float maxValue, float incrementRate, long incDelayValue, boolean switchDirection, long numberOfFullFadeCycles) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.incrementRate = incrementRate;
        this.incDelayValue = incDelayValue;
        this.switchDirection = switchDirection;
        this.incDelay = new Delay();
        this.fadeValue = minValue;
        this.direction = true;
        this.numberOfFullFadeCycles = numberOfFullFadeCycles*2;
        this.fullFadeCyclesCounter = 0;
        this.activityDelay = new Delay();
    }
    
    public float fade(){
        boolean fullCycle = false;
        reset();
        this.activityDelay.start();
        if(this.incDelay.actionAndUpdate(this.incDelayValue) && (this.fullFadeCyclesCounter < this.numberOfFullFadeCycles || this.numberOfFullFadeCycles == 0)){
            if(this.fadeValue < this.maxValue && this.direction == true){
                this.fadeValue += this.incrementRate;
                
                if(this.fadeValue >= this.maxValue){
                    this.fadeValue = this.maxValue;
                    fullCycle = true;
                    
                    if(this.switchDirection){
                        this.direction = !this.direction;
                    }else{
                        this.fadeValue = this.minValue;
                    }
                }
            }else if(this.fadeValue > this.minValue && this.direction == false && (this.fullFadeCyclesCounter < this.numberOfFullFadeCycles || this.numberOfFullFadeCycles == 0)){
                this.fadeValue -= this.incrementRate;
                
                if(this.fadeValue <= this.minValue){
                    this.fadeValue = this.minValue;
                    fullCycle = true;
                    
                    if(this.switchDirection){
                        this.direction = !this.direction;
                    }else{
                        this.fadeValue = this.maxValue;
                    }
                }
            }
            if(fullCycle){
                this.fullFadeCyclesCounter++;
            }
        }
        else if(this.fullFadeCyclesCounter >= this.numberOfFullFadeCycles && this.numberOfFullFadeCycles != 0){
            this.fadeValue = 0;
        }
        
        return this.fadeValue;
    }
    
    /**
     * if there is no activity, the fullFadeCyclesCounter will reset as it would mean that the selector is not being displayed
     */
    private void reset(){
        if(this.activityDelay.action(200)){
            this.fullFadeCyclesCounter = 0;
        }
    }
}
