package cw1_processing.BusinessLogic.Animations;

import cw1_processing.BusinessLogic.Delay;
import cw1_processing.BusinessLogic.Shapes.Image;
import fisica.FWorld;
import processing.core.PApplet;

/**
 *
 * @author Mark
 */
public class Transition{
    //canvas
    private final PApplet parent;
    private final FWorld world;
    
    //fixed config
    private final long transitionDelay;
    private final int numberOfImages;
    private final long numberOfLoops;
    private final float opacity;
    private final float offsetFromSelector;
    
    
    //fixed image string
    private final String imagePath;
    private final String imageName;
    
    //changing variables
    private int loopCounter;
    private long numberOfFullLoops;
    private Delay changeDelay;
    private String imageToLoad;
    
    public Transition(PApplet parent, FWorld world, String imagePath, 
            String imageName, float opacity, int numberOfImages, 
            long transitionDelay, long numberOfLoops, float offsetFromSelector) {
        this.parent = parent;
        this.world = world;
        
        this.loopCounter = 0;
        this.numberOfFullLoops = 0;
        this.numberOfImages = numberOfImages;
        this.transitionDelay = transitionDelay;
        this.changeDelay = new Delay();
        this.numberOfLoops = numberOfLoops;
        this.opacity = opacity;
        this.offsetFromSelector = offsetFromSelector;
        
        this.imagePath = imagePath;
        this.imageName = imageName;
        this.imageToLoad = imagePath+imageName+"0.png";
    }
    
    public void draw(float screenX, float screenY){
        if(this.numberOfFullLoops < this.numberOfLoops){
            loopImages();
            Image arrowup = new Image(this.parent, this.imageToLoad);
            arrowup.tint(this.opacity);
            arrowup.setPosition(screenX, screenY - ((arrowup.getImageHeight()/2)
                    +this.offsetFromSelector));
            this.world.add(arrowup);
        }
    }
    
    
    private void loopImages(){
        if((this.numberOfFullLoops < this.numberOfLoops || this.numberOfLoops == 0)
                && this.changeDelay.actionAndUpdate(this.transitionDelay)){
            if(this.loopCounter < this.numberOfImages-1){
                this.loopCounter++;
            }else{
                this.numberOfFullLoops++;
                this.loopCounter = 0;
            }
            this.imageToLoad = this.imagePath + this.imageName + this.loopCounter + ".png";
        }
        
    }
    
    public void reset(){
        this.numberOfFullLoops = 0;
    }
}
