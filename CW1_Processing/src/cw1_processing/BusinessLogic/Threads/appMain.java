package cw1_processing.BusinessLogic.Threads;

import processing.core.*;
import cw1_processing.BusinessLogic.Threads.Arduino.dispenserAction;
import cw1_processing.BusinessLogic.Delay;
import cw1_processing.BusinessLogic.Player.Avatar;
import cw1_processing.BusinessLogic.Animations.SwipeInterface;
import cw1_processing.BusinessLogic.Shapes.Selector;
import cw1_processing.BusinessLogic.Shapes.ShapeBuilder;
import cw1_processing.BusinessLogic.Sketch.SketchLoader;
import cw1_processing.Constants;
import cw1_processing.SharedBuffer;
import cw1_processing.GameState;
import java.util.ArrayList;
//Fisica Javadoc - http://www.ricardmarxer.com/fisica/reference/
import fisica.*;
//tuio javadoc - ^
import TUIO.*;
import cw1_processing.BusinessLogic.ErrorHandling;
import cw1_processing.BusinessLogic.Sketch.ShapeColour;
import cw1_processing.BusinessLogic.Sketch.ShapeType;

/**
 * @author Mark Galea, Jean Gatt, Lawrence Saliba
 */
public class appMain extends PApplet implements TUIO.TuioListener, SwipeInterface {
    
    private TuioProcessing tuioClient;
    private int instancePlayerId;
    private boolean firstRunSetup = true;
    
    private final boolean verbose = false; // print console debug messages
    private final boolean callback = false; // updates only after callbacks
    
    private int windowWidth;
    private int windowHeight;
    
    //my properties
    private FWorld dynamicWorld;
    private FWorld staticWorld;
    private Delay staticWorldInitDelay;
    private boolean updateStaticWorldFlag;
    private GameState oldWizardState;
    
    private String selectedAvatarId;
    private int avatarSelector;
    private Selector selector;
    private boolean avatarSelectorPlaced;
    
    @Override
    public void settings() {
        size(displayWidth, displayHeight);
    }
    
    @Override
    public void setup() {
        //screen orientation
        if (Constants.getInstance().screenOrientationPortriat) {
            this.windowWidth = height;
            this.windowHeight = width;
        } else {
            this.windowWidth = width;
            this.windowHeight = height;
        }
        
        Fisica.init(this);
        this.staticWorld = new FWorld();
        this.staticWorld.setGravity(0, 0);
        this.dynamicWorld = new FWorld();
        this.dynamicWorld.setGravity(0, 0);
        
        this.staticWorldInitDelay = new Delay();
        this.updateStaticWorldFlag = true;
        //Choosing the blank avatar for now
        this.selectedAvatarId = SharedBuffer.getInstance().getAvatarIDList().get(0);
        
        /* This is done to connects to the port only during the first setup
        as it will be already in use, catering for gam restarts*/
        if (this.firstRunSetup) {
            //Start the client based on the port passed when starting the PApplet
            this.tuioClient = new TuioProcessing(this, Integer.parseInt(args[0]));
            this.firstRunSetup = false;
        }
        
        //store the number of the instance which is passed as a parameter to this class
        this.instancePlayerId = Integer.parseInt(args[1]);
        
        //get the fiducial id passed from the constants
        this.avatarSelector = Constants.getInstance().avatarSelectorfiducialIdMultiplier;
        this.avatarSelectorPlaced = false;
        this.selector = new Selector(this,
                this.dynamicWorld,
                this.instancePlayerId,
                Constants.getInstance().avatarSelectorSize,
                Constants.getInstance().avatarIconsOffsetFromSelector,
                Constants.getInstance().avatarIconsOffsetFromEachOther,
                Constants.getInstance().avatarSelectorSwipeLength,
                SharedBuffer.getInstance().getAvatarIDList()
        );
        
        smooth();
//        noCursor();
        noStroke();
        frameRate(Constants.getInstance().fps);
    }
    
    @Override
    public void draw() {
        background(255);
        
        //screen orientation
        if (Constants.getInstance().screenOrientationPortriat) {
            pushMatrix();
            translate(width, 0);
            rotate(radians(90));
        }
        
        loopStaticWorld();
        loopDynamicWorld();
        
        //screen orientation
        if (Constants.getInstance().screenOrientationPortriat) {
            popMatrix();
        }
    }
    
    /**
     * This will draw the static world
     */
    private void loopStaticWorld() {
        try {
            this.staticWorld.step();
            this.staticWorld.draw(this);
            
            if ((this.updateStaticWorldFlag) && this.staticWorldInitDelay.action(Constants.getInstance().setupDelay)) {
                this.staticWorld.clear();
                switch (SharedBuffer.getInstance().getGameStep()) {
                    case AvatarSelection: {
                        switch (SharedBuffer.getInstance().getPlayerWizardStep(this.instancePlayerId)) {
                            case AvatarSelection: {
                                //the first item in the avatarlist is always the blank avatar
                                Avatar avatar = new Avatar(this.selectedAvatarId);
                                avatar.display(this, this.staticWorld);
                                break;
                            }
                            case Waiting: {
                                //get the avatar id from the respective player
                                Avatar avatar = new Avatar(SharedBuffer.getInstance().getPlayerAvatarID(this.instancePlayerId));
                                avatar.display(this, this.staticWorld);
                                break;
                            }
                        }
                        break;
                    }
                    
                    case GameInPrgress: {
                        //get the avatar id from the respective player
                        Avatar avatar = new Avatar(SharedBuffer.getInstance().getPlayerAvatarID(this.instancePlayerId));
                        avatar.display(this, this.staticWorld);
                        
                        //load the timer
                        SharedBuffer.getInstance().getTimer().diaplayStatic(this, this.staticWorld, this.windowWidth);
                        
                        SketchLoader.getSketch(SharedBuffer.getInstance().getCurrentSketchId()).
                                draw(this, this.staticWorld, this.windowWidth, this.windowHeight);
                        break;
                    }
                    
                    case GameOver: {
                        
                        //get the avatar id from the respective player
                        Avatar avatar = new Avatar(SharedBuffer.getInstance().getPlayerAvatarID(this.instancePlayerId));
                        avatar.display(this, this.staticWorld);
                        
                        switch (SharedBuffer.getInstance().getPlayerWizardStep(this.instancePlayerId)) {
                            case TimeUp: {
                                addImage(
                                        this.staticWorld,
                                        AddImage.SCREEN_CENTER,
                                        Constants.getInstance().textImagesLocation + Constants.getInstance().gameStateImageTimeUp + ".png"
                                );
                                break;
                            }
                            case GameOver: {
                                addImage(
                                        this.staticWorld,
                                        AddImage.SCREEN_CENTER,
                                        Constants.getInstance().textImagesLocation + Constants.getInstance().gameStateImageGameOver + ".png"
                                );
                                break;
                            }
                            case Win: {
                                addImage(
                                        this.staticWorld,
                                        AddImage.SCREEN_CENTER,
                                        Constants.getInstance().textImagesLocation + Constants.getInstance().gameStateImageWin + ".png"
                                );
                                
                                //dispense sweet
                                if (Constants.getInstance().waitForArduino) {
                                    
                                    switch (this.instancePlayerId) {
                                        case 0: {
                                            SharedBuffer.getInstance().getArduino().dispenser(dispenserAction.ONE);
                                            break;
                                        }
                                        case 1: {
                                            SharedBuffer.getInstance().getArduino().dispenser(dispenserAction.TWO);
                                            break;
                                        }
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
                
//            System.out.println("player: " + this.instancePlayerId + " game ->"+ SharedBuffer.getInstance().getGameStep());
//            System.out.println("player: " + this.instancePlayerId + " player ->"+ SharedBuffer.getInstance().getPlayerWizardStep(this.instancePlayerId));
                this.oldWizardState = SharedBuffer.getInstance().getGameStep();
                this.updateStaticWorldFlag = false;
            }
        } catch (Exception ex) {
            ErrorHandling.OutputException(ex);
        }
    }
    
    /**
     * This will draw the dynamic world
     */
    private void loopDynamicWorld() {
        try {
            this.dynamicWorld.step();
            this.dynamicWorld.draw(this);
            
//            FSemiCircle c = new FSemiCircle(this, 150);
//            c.setPosition(150, 150);
//            c.setFill(200,200,200);
//            this.dynamicWorld.add(c);
            
            //list of tuio object from camera
            ArrayList<TuioObject> tuioObjectList = tuioClient.getTuioObjectList();
            
            //update the static world when the player is set to do so
            if (SharedBuffer.getInstance().getPlayerUpdateStaticWorld(this.instancePlayerId)) {
                this.updateStaticWorldFlag = true;
                SharedBuffer.getInstance().setPlayerUpdateStaticWorld(this.instancePlayerId, false);
            }
            
            //reset game
            if (SharedBuffer.getInstance().isResetGame(this.instancePlayerId)) {
                setup();
                SharedBuffer.getInstance().setResetedGame(this.instancePlayerId);
                SharedBuffer.getInstance().allReseted();
            }
            
            //game wizard step
            switch (SharedBuffer.getInstance().getGameStep()) {
                case AvatarSelection: {
                    //player wizard step
                    switch (SharedBuffer.getInstance().getPlayerWizardStep(this.instancePlayerId)) {
                        case AvatarSelection: {
                            this.avatarSelectorPlaced = false;
                            for (TuioObject tobj : tuioObjectList) {
                                if (tobj.getSymbolID() == this.avatarSelector) {
                                    this.avatarSelectorPlaced = true;
                                    //screen orientation
                                    if (Constants.getInstance().screenOrientationPortriat) {
                                        //place the selector on screen and get the selected id out of it
                                        this.selectedAvatarId = this.selector.display(
                                                tobj.getScreenY(this.windowWidth),
                                                tobj.getScreenX(this.windowHeight),
                                                tobj.getAngle()
                                        );
                                    } else {
                                        //place the selector on screen and get the selected id out of it
                                        this.selectedAvatarId = this.selector.display(
                                                tobj.getScreenX(this.windowWidth),
                                                tobj.getScreenY(this.windowHeight),
                                                tobj.getAngle()
                                        );
                                    }
                                    //update the static world everytime there is a change
                                    this.updateStaticWorldFlag = true;
                                }
                            }
                            
                            //if the select is not placed: Please place the selector
                            if (tuioObjectList.isEmpty() || !this.avatarSelectorPlaced) {
                                addImage(
                                        this.dynamicWorld,
                                        AddImage.SCREEN_CENTER,
                                        Constants.getInstance().textImagesLocation + Constants.getInstance().placeSelectorTextImage + ".png"
                                );
                            }
                            
                            break;
                        }
                        case Waiting: {
                            //waiting for other player image
                            addImage(
                                    this.dynamicWorld,
                                    AddImage.LOGO,
                                    Constants.getInstance().textImagesLocation + Constants.getInstance().waitingForOtherPlayerTextImage + ".png"
                            );
                            
                            //wait for both players to be ready
                            //Once both players are ready and waiting:
                            //update static world
                            //set the game wizard to game in progress
                            //start the timer (now redundant left it there just to make sure noting goes wrong)
                            //set the game to start
                            if (SharedBuffer.getInstance().readyCheck()) {
                                SharedBuffer.getInstance().setUpdateStaticWorlds();
                                SharedBuffer.getInstance().setGameStep(GameState.GameInPrgress);
                                SharedBuffer.getInstance().getTimer().startGame();
                                SharedBuffer.getInstance().gameStartDelayStart();
                            }
                            break;
                        }
                    }
                    break;
                }
                
                case GameInPrgress: {
                    //this will fix bug when both players ready up at the same time
                    if (SharedBuffer.getInstance().gameStartDelayAction(Constants.getInstance().gameStartDelay)) {
                        
                        //display the digits of the timer
                        //if time up, set players time up and update static worlds
                        if (SharedBuffer.getInstance().getTimer().displayDynamic(this, this.dynamicWorld, this.windowWidth)) {
                            SharedBuffer.getInstance().setPlayersGameTimeUp();
                            SharedBuffer.getInstance().setUpdateStaticWorlds();
                            SharedBuffer.getInstance().setGameStep(GameState.GameOver);
                        }
                        
                        if (ShapeBuilder.checkWin(this.instancePlayerId)) {
                            SharedBuffer.getInstance().setPlayerWizardStep(this.instancePlayerId, GameState.Win);
                            //check if this player has won, if s/he did set the others to game over.
                            //set the gamestep to game over
                            SharedBuffer.getInstance().setOtherPlayersGameOver(this.instancePlayerId);
                            SharedBuffer.getInstance().setUpdateStaticWorlds();
                            SharedBuffer.getInstance().setGameStep(GameState.GameOver);
                        }
                        
                        //get the tuio objects and perform the checks etc
                        for (TuioObject tobj : tuioObjectList) {
                            
                            //screen orientation
                            float objectX = tobj.getScreenX(this.windowWidth);
                            float objectY = tobj.getScreenY(this.windowHeight);
                            if (Constants.getInstance().screenOrientationPortriat) {
                                objectX = tobj.getScreenY(this.windowWidth);
                                objectY = tobj.getScreenX(this.windowHeight);
                            }
                            //temp - this will set this player as winner
                            /*if (tobj.getSymbolID() == 8) {
                            SharedBuffer.getInstance().setPlayerWizardStep(this.instancePlayerId, GameState.Win);
                            } //place objects on screen
                            else if (tobj.getSymbolID() == 1) {
                            ShapeBuilder.createTriangle(this.dynamicWorld, this.instancePlayerId, objectX, objectY, tobj.getAngle(), tobj.getSymbolID());
                            }
                            else if (tobj.getSymbolID() == 3) {
                            ShapeBuilder.createRectangle(this.dynamicWorld, this.instancePlayerId, objectX, objectY, tobj.getAngle(), tobj.getSymbolID());
                            }
                            else if (tobj.getSymbolID() % 2 == 0) {
                            ShapeBuilder.createSquare(this.dynamicWorld, this.instancePlayerId, objectX, objectY, tobj.getAngle(), tobj.getSymbolID());
                            } else {
                            ShapeBuilder.createCircle(this.dynamicWorld, this.instancePlayerId, objectX, objectY, tobj.getSymbolID());
                            }*/
                            
                            if (tobj.getSymbolID() >= 10 && tobj.getSymbolID() < 20) {
                                //Shapes
                                switch (ShapeType.retrieveShapeTypeViaFiducialId(tobj.getSymbolID())) {
                                    case CIRCLE:
                                        ShapeBuilder.createCircle(this.dynamicWorld,
                                                this.instancePlayerId, objectX, objectY,
                                                tobj.getSymbolID());
                                        break;
                                    case RECTANGLE:
                                        ShapeBuilder.createRectangle(this.dynamicWorld,
                                                this.instancePlayerId, objectX, objectY,
                                                tobj.getAngle(), tobj.getSymbolID());
                                        break;
                                    case SQUARE:
                                        ShapeBuilder.createSquare(this.dynamicWorld,
                                                this.instancePlayerId, objectX, objectY,
                                                tobj.getAngle(), tobj.getSymbolID());
                                        break;
                                    case TRIANGLE:
                                        ShapeBuilder.createTriangle(this.dynamicWorld,
                                                this.instancePlayerId, objectX, objectY,
                                                tobj.getAngle(), tobj.getSymbolID());
                                        break;
                                    case SEMICIRCLE:
                                        ShapeBuilder.createSemiCircle(this, this.dynamicWorld,
                                                this.instancePlayerId, objectX, objectY,
                                                tobj.getAngle(), tobj.getSymbolID());
                                        break;
                                    default:
                                        //not available
                                        break;
                                }
                            } else if (tobj.getSymbolID() >= 20 && tobj.getSymbolID() < 30) {
                                //Crayons
                                ShapeBuilder.createCrayon(this, this.dynamicWorld,
                                        ShapeColour.retrieveShapeColourViaFiducialId(tobj.getSymbolID())
                                        , this.instancePlayerId,
                                        objectX, objectY, tobj.getAngle(), tobj.getSymbolID());
                            }
                        }
                    } else {
                        //reset the timer so that when the game starts the timer will represent the value set in the config file
                        SharedBuffer.getInstance().getTimer().startGame();
                    }
                    break;
                }
                
                case GameOver: {
                    
                    //restart the game
                    for (TuioObject tobj : tuioObjectList) {
                        if (tobj.getSymbolID() == 7) {
                            SharedBuffer.getInstance().resetGameAllPlayers();
                        }
                    }
                    break;
                }
            }
            this.dynamicWorld.clear();
        } catch (Exception ex) {
            ErrorHandling.OutputException(ex);
        }
    }
    
    // --------------------------------------------------------------
    //Helper functionality
    // --------------------------------------------------------------
    //Helper methods to check
    @Override
    //change the player status to waiting
    public void selected(int playerID) {
        SharedBuffer.getInstance().setPlayerAvatarID(playerID, selectedAvatarId);
    }
    
    /**
     * add a new image to this appMain
     *
     * @param world
     * @param type location where to place it
     * @param path path of image
     */
    private void addImage(FWorld world, AddImage type, String path) {
        FBox text = new FBox(0, 0);
        text.setStatic(true);
        PImage img = loadImage(path);
        text.attachImage(img);
        if (type == AddImage.LOGO) {
            text.setPosition(30 + (img.width / 2), 100 + (img.height / 2));
        } else {
            text.setPosition(this.windowWidth / 2, this.windowHeight / 2);
        }
        world.add(text);
    }
    
// --------------------------------------------------------------
// these callback methods are called whenever a TUIO event occurs
// there are three callbacks for add/set/del events for each object/cursor/blob type
// the final refresh callback marks the end of each TUIO frame
// called when an object is added to the scene
    @Override
    public void addTuioObject(TuioObject tobj) {
        if (verbose) {
            println("add obj " + tobj.getSymbolID() + " (" + tobj.getSessionID() + ") " + tobj.getX() + " " + tobj.getY() + " " + tobj.getAngle());
        }
    }
    
// called when an object is moved
    @Override
    public void updateTuioObject(TuioObject tobj) {
        if (verbose) {
            println("set obj " + tobj.getSymbolID() + " (" + tobj.getSessionID() + ") " + tobj.getX() + " " + tobj.getY() + " " + tobj.getAngle()
                    + " " + tobj.getMotionSpeed() + " " + tobj.getRotationSpeed() + " " + tobj.getMotionAccel() + " " + tobj.getRotationAccel());
        }
    }
    
// called when an object is removed from the scene
    @Override
    public void removeTuioObject(TuioObject tobj) {
        if (verbose) {
            println("del obj " + tobj.getSymbolID() + " (" + tobj.getSessionID() + ")");
        }
        
    }
    
// --------------------------------------------------------------
// called when a cursor is added to the scene
    @Override
    public void addTuioCursor(TuioCursor tcur) {
        if (verbose) {
            println("add cur " + tcur.getCursorID() + " (" + tcur.getSessionID() + ") " + tcur.getX() + " " + tcur.getY());
        }
//redraw();
    }
    
// called when a cursor is moved
    @Override
    public void updateTuioCursor(TuioCursor tcur) {
        if (verbose) {
            println("set cur " + tcur.getCursorID() + " (" + tcur.getSessionID() + ") " + tcur.getX() + " " + tcur.getY()
                    + " " + tcur.getMotionSpeed() + " " + tcur.getMotionAccel());
        }
//redraw();
    }
    
// called when a cursor is removed from the scene
    @Override
    public void removeTuioCursor(TuioCursor tcur) {
        if (verbose) {
            println("del cur " + tcur.getCursorID() + " (" + tcur.getSessionID() + ")");
        }
//redraw()
    }
    
// --------------------------------------------------------------
// called when a blob is added to the scene
    @Override
    public void addTuioBlob(TuioBlob tblb) {
        if (verbose) {
            println("add blb " + tblb.getBlobID() + " (" + tblb.getSessionID() + ") " + tblb.getX() + " " + tblb.getY() + " " + tblb.getAngle() + " " + tblb.getWidth() + " " + tblb.getHeight() + " " + tblb.getArea());
        }
//redraw();
    }
    
// called when a blob is moved
    @Override
    public void updateTuioBlob(TuioBlob tblb) {
        if (verbose) {
            println("set blb " + tblb.getBlobID() + " (" + tblb.getSessionID() + ") " + tblb.getX() + " " + tblb.getY() + " " + tblb.getAngle() + " " + tblb.getWidth() + " " + tblb.getHeight() + " " + tblb.getArea()
                    + " " + tblb.getMotionSpeed() + " " + tblb.getRotationSpeed() + " " + tblb.getMotionAccel() + " " + tblb.getRotationAccel());
        }
//redraw()
    }
    
// called when a blob is removed from the scene
    @Override
    public void removeTuioBlob(TuioBlob tblb) {
        if (verbose) {
            println("del blb " + tblb.getBlobID() + " (" + tblb.getSessionID() + ")");
        }
//redraw()
    }
    
// --------------------------------------------------------------
// called at the end of each TUIO frame
    @Override
    public void refresh(TuioTime frameTime) {
        if (verbose) {
            println("frame #" + frameTime.getFrameID() + " (" + frameTime.getTotalMilliseconds() + ")");
        }
        if (callback) {
            redraw();
        }
    }
}
