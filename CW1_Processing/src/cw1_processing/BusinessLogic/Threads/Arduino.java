package cw1_processing.BusinessLogic.Threads;

import cw1_processing.BusinessLogic.ErrorHandling;
import cw1_processing.Constants;
import org.zu.ardulink.Link;

/**
 * @author Mark
 */

public class Arduino implements Runnable{
    private boolean connected;
    private String arduinoPort;
    private Link link;
    
    private boolean dispenserOne;
    private boolean dispenserTwo;
    private boolean calibrateDispenserOne;
    private boolean calibrateDispenserTwo;
    private boolean vibratorOne;
    private boolean vibratorTwo;
    private int durationVibratorOne;
    private int durationVibratorTwo;
    private int intensityVibratorOne;
    private int intensityVibratorTwo;
    
    
    public static enum dispenserAction {ONE, TWO, CALIBRATE_ONE, CALIBRATE_TWO};
    public static enum vibratorAction{VIBRATOR_ONE, VIBRATOR_TWO}
    
    public Arduino(String arduinoPort){
        this.dispenserOne = false;
        this.dispenserTwo = false;
        this.calibrateDispenserOne = false;
        this.calibrateDispenserTwo = false;
        this.vibratorOne = false;
        this.vibratorTwo = false;
        this.durationVibratorOne = 0;
        this.durationVibratorTwo = 0;
        this.intensityVibratorOne = 0;
        this.intensityVibratorTwo = 0;
        
        this.arduinoPort = arduinoPort;
    }
    
    /**
     * This method holds the functionality that will be looped with the arduino.
     * @param delay
     * @throws InterruptedException
     */
    private void arduinoInternalfunctionality(int delay) throws InterruptedException{
//        this.link.sendPowerPinIntensity(10, this.led);
//        this.link.sendCustomMessage(String.format("servo/%s?", this.servoAngle));
        
        if(this.dispenserOne){
            //this sends this command alp://cust servo/1?
            this.link.sendCustomMessage("servo/1?");
            this.dispenserOne = false;
        }
        
        if(this.dispenserTwo){
            //this sends this command alp://cust servo/2?
            this.link.sendCustomMessage("servo/2?");
            this.dispenserTwo = false;
        }
        
        if(this.calibrateDispenserOne){
            //this sends this command alp://cust cal_servo/1?
            this.link.sendCustomMessage("cal_servo/1?");
            this.calibrateDispenserOne = false;
        }
        
        if(this.calibrateDispenserTwo){
            //this sends this command alp://cust cal_servo/2?
            this.link.sendCustomMessage("cal_servo/2?");
            this.calibrateDispenserTwo = false;
        }
        
        if(this.vibratorOne){
            //this sends this command alp://cust vib/1?1000#255^
            this.link.sendCustomMessage("vib/1?"+this.durationVibratorOne+"#"+this.intensityVibratorOne+"^");
            this.durationVibratorOne = 0;
            this.intensityVibratorOne = 0;
            this.vibratorOne = false;
        }
        
        if(this.vibratorTwo){
            //this sends this command alp://cust vib/2?1000#255^
            this.link.sendCustomMessage("vib/2?"+this.durationVibratorTwo+"#"+this.intensityVibratorTwo+"^");
            this.durationVibratorTwo = 0;
            this.intensityVibratorTwo = 0;
            this.vibratorTwo = false;
        }
//        this.link.addRawDataListener(new RawDataListener() {
//
//            @Override
//            public void parseInput(String id, int numBytes, int[] message) {
//                StringBuilder build = new StringBuilder(numBytes + 1);
//                for (int i = 0; i < numBytes; i++) {
//                    build.append((char)message[i]);
//                }
//                System.out.println(build.toString());
//            }
//        });
    }
    
    @Override
    public void run() {
        try {
            
            this.link = Link.getDefaultInstance();
            
            
            if (this.arduinoPort != null){
                this.connected = this.link.connect(this.arduinoPort);
                
                if(this.connected){
                    //this is need to wait for the arduino to settle down
                    Thread.sleep(2000);
                    
                    while(true){
                        arduinoInternalfunctionality(10);
                    }
                    
                }else{
                    ErrorHandling.OutputError(String.format("Failed to Connect to arduino on %s",this.arduinoPort));
                }
            }else{
                ErrorHandling.OutputError(
                        String.format("Not A valid port, modify the port in %s",Constants.ConfigFileLocation));
            }
            
        }
        catch(Exception ex) {
            ErrorHandling.OutputException(ex);
        }
    }
    
    /**
     * Method used to give instructions to the dispenser
     * @param action
     */
    public synchronized void dispenser(dispenserAction action){
        switch(action){
            case ONE:
                this.dispenserOne = true;
                break;
            case TWO:
                this.dispenserTwo = true;
                break;
            case CALIBRATE_ONE:
                this.calibrateDispenserOne = true;
                break;
            case CALIBRATE_TWO:
                this.calibrateDispenserTwo = true;
                break;
            default:
                ErrorHandling.OutputError("Dispenser Option NOT supported!");
        }
    }
    
    public synchronized void vibrator(vibratorAction action, int intensity, int duration){
        switch(action){
            case VIBRATOR_ONE:
                this.vibratorOne = true;
                this.durationVibratorOne = duration;
                this.intensityVibratorOne = intensity;
                break;
            case VIBRATOR_TWO:
                this.vibratorTwo = true;
                this.durationVibratorTwo = duration;
                this.intensityVibratorTwo = intensity;
                break;
            default:
                ErrorHandling.OutputError("Dispenser Option NOT supported!");
        }
    }
//    public static synchronized void dispenserOne(){
//        Arduino.dispenserOne = true;
//    }
    
//    public static synchronized void dispenserTwo(){
//        Arduino.dispenserTwo = true;
//    }
//
//    public static synchronized void calibrateDispenserOne(){
//        Arduino.calibrateDispenserOne = true;
//    }
//
//    public static synchronized void calibrateDispenserTwo(){
//        Arduino.calibrateDispenserTwo = true;
//    }
    
    /**
     * Check if the Arduino is connected
     * @return
     */
    public synchronized boolean isConnected(){
        return this.connected || !Constants.getInstance().waitForArduino;
    }
}
