/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package cw1_processing.BusinessLogic.Threads;

import cw1_processing.BusinessLogic.ErrorHandling;
import cw1_processing.Constants;
import java.io.FileInputStream;
import javazoom.jl.player.*;

/**
 *
 * @author jeangatt
 */
public class Sound implements Runnable {
    public enum play {NONE, WOODENBLOCK};
    
    private String toPlay;
    public Sound(play name) {
        switch(name){
            case WOODENBLOCK:
            {
                this.toPlay = Constants.getInstance().woodenBlockSoundLocation;
                break;
            }
        }
    }
    
    
    @Override
    public void run() {
        if(!this.toPlay.isEmpty() && this.toPlay != null){
            
            try {
                FileInputStream fis = new FileInputStream(this.toPlay);
                Player playMP3 = new Player(fis);
                playMP3.play();
                
            } catch (Exception e) {
                ErrorHandling.OutputException(e);
            }
        }
    }
}