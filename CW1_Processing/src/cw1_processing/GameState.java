/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw1_processing;

/**
 *
 * @author Mark
 */
public enum GameState {
    AvatarSelection,
    Waiting,
    GameInPrgress,
    TimeUp,
    Win,
    GameOver
}
