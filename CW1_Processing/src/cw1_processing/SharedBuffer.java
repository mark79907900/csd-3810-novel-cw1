package cw1_processing;

import cw1_processing.BusinessLogic.Delay;
import cw1_processing.BusinessLogic.Threads.Arduino;
import cw1_processing.BusinessLogic.Player.Player;
import cw1_processing.BusinessLogic.Player.ResetGame;
import cw1_processing.BusinessLogic.Player.Timer;
import cw1_processing.BusinessLogic.Shapes.ShapePlayerColour;
import cw1_processing.BusinessLogic.Sketch.SketchLoader;
import cw1_processing.BusinessLogic.ThreadPool;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Mark
 */
public class SharedBuffer implements UpdateStaticWorld {
    
    private static volatile SharedBuffer self = null;
    private GameState gameState;
    private List<Player> players;
    private List<String> avatarIDs;
    private List<ShapePlayerColour> shapePlayerColours;
    private Arduino arduino = null;
    private Timer timer;
    private int currentSketchId;
    private Delay gameStartDelay;
    
    private SharedBuffer() {
        
        //check if to start arduino
        if (Constants.getInstance().waitForArduino) {
            //push the arduino service in the thread pool
            this.arduino = new Arduino(Constants.getInstance().arduinoPort);
            ThreadPool.pushThread(this.arduino);
        }
        
        //get the list of the avatars that should be displayed around the selector
        this.avatarIDs = new ArrayList<>(Arrays.asList(Constants.getInstance().avatarIDList.split(",")));
        
        //init list of shape, players and colours all linked
        this.shapePlayerColours = new ArrayList<>();
        
        //init the gameStep to avatarSelection
        this.gameState = GameState.AvatarSelection;
        
        //make a list of players and populate it
        this.players = new ArrayList<>();
        for (int i = 0; i < Constants.getInstance().numberOfPlayers; i++) {
            this.players.add(new Player());
        }
        
        //show the timer digits
        this.timer = new Timer(this, Constants.getInstance().duration, Constants.getInstance().countdownEffect);
        
        //delay the starting process, this is used to wait for the threads to finish their task.
        //it reduces a large possibility of bugs
        this.gameStartDelay = new Delay();
        
        //Setting currentSKetchId
//        this.currentSketchId = randomChooseSketchId(SketchLoader.getSketches().length);
        this.currentSketchId = 2;
        
    }
    
    /**
     * Init the buffer by creating the only instance possible
     */
    public static void init() {
        if (SharedBuffer.self == null) {
            SharedBuffer.self = new SharedBuffer();
        }
    }
    
    /**
     * Remove the current instance of the shared buffer and create a new one
     * This is used to reset the game
     */
    public static void reset() {
        SharedBuffer.self = null;
        SharedBuffer.init();
        SketchLoader.reset();
    }
    
    /**
     * get the instance of the singleton
     *
     * @return
     */
    public static SharedBuffer getInstance() {
        init();
        return SharedBuffer.self;
    }
    
    /**
     * Get the arduino instance
     *
     * @return
     */
    public Arduino getArduino() {
        return this.arduino;
    }
    
    /**
     * Get the instance of the timer
     *
     * @return
     */
    public Timer getTimer() {
        return timer;
    }
    
    /**
     * Check that both players are waiting for the game to start
     *
     * @return true if both players are waiting
     */
    public boolean readyCheck() {
        return (this.players.get(0).getPlayerState() == GameState.Waiting
                && this.players.get(1).getPlayerState() == GameState.Waiting);
    }
    
    /**
     * check if both players are at Wizard.GameInPrgress
     *
     * @return
     */
    public boolean gameInProgressCheck() {
        return (this.players.get(0).getPlayerState() == GameState.GameInPrgress
                && this.players.get(1).getPlayerState() == GameState.GameInPrgress);
    }
    
    /**
     * Get the current game step wizard value
     *
     * @return
     */
    public GameState getGameStep() {
        return this.gameState;
    }
    
    /**
     * Shift the game to another wizard step to move towards another phase of
     * the game
     *
     * @param gameStep
     */
    public void setGameStep(GameState gameStep) {
        this.gameState = gameStep;
    }
    
    /**
     * Start the delay between the waiting phase and game in progress
     */
    public void gameStartDelayStart() {
        this.gameStartDelay.start();
    }
    
    /**
     * Check if it is time to start the game
     *
     * @param diff the time in millis to wait before starting
     * @return
     */
    public boolean gameStartDelayAction(long diff) {
        return this.gameStartDelay.action(diff);
    }
    
    /**
     * set the avatar of the player with the passed ids. also switch the player
     * to waiting mode
     *
     * @param playerID
     * @param avatarID
     */
    public void setPlayerAvatarID(int playerID, String avatarID) {
        this.players.get(playerID).setAvatarID(avatarID);
        this.players.get(playerID).setWizzardState(GameState.Waiting);
    }
    
    /**
     * Get the player's avatar id
     *
     * @param playerID
     * @return
     */
    public String getPlayerAvatarID(int playerID) {
        return this.players.get(playerID).getAvatarID();
    }
    
    /**
     * get the list of avatars
     *
     * @return
     */
    public List<String> getAvatarIDList() {
        return this.avatarIDs;
    }
    
    /**
     * Get the wizard step for the respective player
     *
     * @param playerID
     * @return
     */
    public GameState getPlayerWizardStep(int playerID) {
        return this.players.get(playerID).getPlayerState();
    }
    
    /**
     * Check if the static world for the current player should be updated
     *
     * @param playerID
     * @return
     */
    public boolean getPlayerUpdateStaticWorld(int playerID) {
        return this.players.get(playerID).isUpdateStaticWorld();
    }
    
    /**
     * flag the static world for the passed player to update or not to update
     *
     * @param playerID
     * @param update
     */
    public void setPlayerUpdateStaticWorld(int playerID, boolean update) {
        this.players.get(playerID).updateStaticWorld(update);
    }
    
    /**
     * Set the player wizard step
     *
     * @param playerID
     * @param playerWizardStep
     */
    public void setPlayerWizardStep(int playerID, GameState playerWizardStep) {
        this.players.get(playerID).setWizzardState(playerWizardStep);
    }
    
    /**
     * Set all instance to reset the game
     */
    public void resetGameAllPlayers() {
        for (Player p : this.players) {
            p.setResetGame(ResetGame.RESET);
        }
    }
    
    /**
     * Once the setup has been reseted, flag the game for that player to reseted
     *
     * @param playerID
     */
    public void setResetedGame(int playerID) {
        this.players.get(playerID).setResetGame(ResetGame.RESETTED);
    }
    
    /**
     * Check if the instance for that player should reset
     *
     * @param playerID
     * @return
     */
    public boolean isResetGame(int playerID) {
        return this.players.get(playerID).getResetGame() == ResetGame.RESET;
    }
    
    /**
     * if all instances of the game are reseted, reset the buffer
     */
    public void allReseted() {
        boolean allReseted = true;
        
        for (Player p : this.players) {
            if (p.getResetGame() != ResetGame.RESETTED) {
                allReseted = false;
            }
        }
        
        if (allReseted) {
            SharedBuffer.reset();
        }
    }
    
    /**
     * set all players to game in progress
     */
    public synchronized void setPlayersGameInProgress() {
        for (Player p : this.players) {
            p.setWizzardState(GameState.GameInPrgress);
        }
    }
    
    /**
     * set all players to time up
     */
    public synchronized void setPlayersGameTimeUp() {
        for (Player p : this.players) {
            p.setWizzardState(GameState.TimeUp);
        }
    }
    
    /**
     * set all the other players apart the one passed to game over
     *
     * @param playerID
     */
    public synchronized void setOtherPlayersGameOver(int playerID) {
        int counter = 0;
        for (Player p : this.players) {
            if (counter != playerID) {
                p.setWizzardState(GameState.GameOver);
            }
            counter++;
        }
    }
    
    /**
     * set all players to update their static world
     */
    public void setUpdateStaticWorlds() {
        for (Player p : this.players) {
            p.updateStaticWorld(true);
        }
    }
    
    /**
     * this is used to call this action from the interface
     */
    @Override
    public void update() {
        setUpdateStaticWorlds();
    }
    
    /**
     * this is used to get the current sketchID
     * @return current sketch id
     */
    public int getCurrentSketchId() {
        return currentSketchId;
    }
    
    /**
     * To load another sketch
     */
    public void updateCurrentSketchId(){
        this.currentSketchId = randomChooseSketchId(SketchLoader.getSketches().length);
    }
    
    /**
     * this chooses a random sketch from the sketches array for both players
     * @param noOfSketches
     */
    public int randomChooseSketchId(int noOfSketches){
        return (int) (Math.random()*noOfSketches)+1;
    }
    
    public List<ShapePlayerColour> getShapePlayerColours() {
        return shapePlayerColours;
    }
    
    public void setShapePlayerColours(List<ShapePlayerColour> shapePlayerColours) {
        this.shapePlayerColours = shapePlayerColours;
    }
    
    public void addShapePlayerColour(ShapePlayerColour spc){
        this.shapePlayerColours.add(spc);
    }
}
