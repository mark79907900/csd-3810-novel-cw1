package cw1_processing;

import cw1_processing.BusinessLogic.Threads.appMain;
import cw1_processing.BusinessLogic.Config;
import cw1_processing.BusinessLogic.Sketch.ShapeColour;
import cw1_processing.BusinessLogic.Sketch.SketchLoader;

public class CW1_Processing {
    
    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        //Get the configs from the config file
        Config.init(Constants.ConfigFileLocation);
        Constants.init();
        setUpCrayonColoursInfo();
        SketchLoader.init();
        SharedBuffer.init();
        
        //check if the arduino is online and then turn on reactivision
        //this could be configured to not wait for arduino in the config file
        //in case you do not have the hardware
        if(SharedBuffer.getInstance().getArduino() != null){
            int waitingCounter = 0;
            while(!SharedBuffer.getInstance().getArduino().isConnected()){
                if(waitingCounter > 5){
                    System.out.println("Failed to connect to arduino, check the config file and try again.\nExiting App");
                    System.exit(0);
                }else{
                    System.out.println(String.format("Waiting for arduino... (%d)", waitingCounter+1));
                }
                waitingCounter++;
                Thread.sleep(1000);
            }
        }
        
        appMain.main(RunString.get(0));
        appMain.main(RunString.get(1));
    }
    
    /*
    This method is used to set up the crayon file location for the shapcolour 
    enums, after the constants would have been initialised.
    */
    private static void setUpCrayonColoursInfo(){
        ShapeColour.BLUE.setImagePath(String.format("%s//%s.png", 
                Constants.getInstance().crayonsImagesLocation, "blue-pencil"));
        ShapeColour.RED.setImagePath(String.format("%s//%s.png", 
                Constants.getInstance().crayonsImagesLocation, "red-pencil"));
        ShapeColour.ORANGE.setImagePath(String.format("%s//%s.png", 
                Constants.getInstance().crayonsImagesLocation, "orange-pencil"));
        ShapeColour.GREEN.setImagePath(String.format("%s//%s.png", 
                Constants.getInstance().crayonsImagesLocation, "green-pencil"));
        ShapeColour.YELLOW.setImagePath(String.format("%s//%s.png", 
                Constants.getInstance().crayonsImagesLocation, "yellow-pencil"));
    }
    
}
