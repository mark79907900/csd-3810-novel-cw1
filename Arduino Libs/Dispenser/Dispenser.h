#ifndef Dispenser_h
#define Dispenser_h

//import arduino library
#if defined(ARDUINO) && ARDUINO >= 100
#include <Arduino.h>
#else
#include <WProgram.h>
#include <pins_arduino.h>
#endif

#include <Servo\Servo.h>
#include <EEPROM\EEPROM.h>

class Dispenser{
private:
	boolean _activate;
	boolean _collected;
	boolean _dispensed;
	int _timesToVibrate;
	int _vibrationCounter;
	Servo _servo;
	int _servoPin;
	int _anglePin;
	int	_dispansedAngle;
	int _collectAngle;
	int _dispansedAnalog;
	int _collectAnalog;
	int _dispenserID;

	/*
	Store 10bit values in the EPROM
	value: the value to be added (limited to 1023, no validation in place)
	startAddress: the address on which it should be stored, note that each address will actually take  addresses (do not exced 100 addresses)
	*/
	boolean StoreEPROM(int value, int startAddress);

	/*
	Read the 10bit EPROM
	valueToRead: the address to read
	*/
	int ReadEPROM(int addressToRead);

	/*
	Clear the EPROM
	*/
	void ClearEPROM();
public:
	/*
	servoPin:	The pin used to attach the servo to
	anglePin:	The analoge input pin used to read the servo angle
	dispenserID:	This id is used to store the data in the arduino EPROM so that multple dispensers are supported
	timesToVibrate:	the duration of the vibrations when dispensing, set to 0 if not needed
	dispenseAngle:	the angle at which the servo will collect the item. this is used for calibration
	*/
	Dispenser(int servoPin, int anglePin, int dispenserID, int timesToVibrate, int collectAngle);

	/*
	This method should be placed in the loop, it will perform the actions as a background process
	*/
	void Loop();

	/*
	This method is used to dispense an item. 
	It returns true if there was no action being processed already.
	It returns false if the dispenser was already dispensing
	Returns: boolean
	*/
	boolean Activate();

	/*
	This method is used to setup the servo.
	calibrate: set to true to perform a calibration on setup, set to false if calibration is not needed
	*/
	void Setup(boolean calibrate);

	/*
	Get the servo object
	Returns Servo
	*/
	Servo GetServo();

	/*
	Method used to calibrate the servo collect angle
	collectAngle: accepts values from 0-180 (there is no validation)
	*/
	void CalibrateDispenseAngle(int collectAngle);

	/*
	Callibrate the dispense and collect analog values manually. 
	This is ideally placed in the setup of the arduino
	*/
	void ManualCalibrationServoAnalog(int dispansedAnalog, int collectAnalog);

	/*
	Automatically callibrate the servo and store the data in the EPROM of the arduino
	*/
	boolean AutoCalibrate();
};

#endif