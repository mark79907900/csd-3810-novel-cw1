#include "Dispenser.h"
#include <Servo\Servo.h>
#include <EEPROM\EEPROM.h>

//Constructor
Dispenser::Dispenser(int servoPin, int anglePin, int dispenserID, int timesToVibrate, int collectAngle){
	_activate = false;
	_collected = false;
	_dispensed = false;
	_timesToVibrate = timesToVibrate;
	_vibrationCounter = 0;
	_servoPin = servoPin;
	_anglePin = anglePin;
	_dispansedAngle = 0;
	_collectAngle = collectAngle;
	_dispansedAnalog = 300;
	_collectAnalog = 1018;
	_dispenserID = dispenserID;
	Servo _servo;
}

void Dispenser::Loop(){
	//if the dispenser is stated to dispens
	if (_activate){

		//get the current servo angle
		int angle = analogRead(_anglePin);

		//if the servo is not dispensed and collected
		if (_collected && !_dispensed){

			//comand the servo to dispense
			_servo.write(_dispansedAngle);

			//check the current angle to see if it has reached dispense state
			if (angle < _dispansedAnalog){

				//if it is at dispense state
				//vibrate if the stipulated amount of vibration has not taken place
				//once vibration is finished flag collected false as the item is dropped and dispensed true as the item was delivered
				if (_vibrationCounter >= _timesToVibrate){
					_collected = false;
					_dispensed = true;
				}
				else{
					//code that performs the vibration
					//note that the angle moved must be within the range of the current angle
					if (_vibrationCounter % 2 == 0){
						_servo.write(_dispansedAngle + 10);
					}
					else{
						_servo.write(_dispansedAngle);
					}
					_vibrationCounter++;
				}

			}

		}
		//if servo is not collected and dispensed or not
		else{
			//turn servo to collect
			_servo.write(_collectAngle);

			//when angle of servo reaches collected state flag collected
			if (angle > _collectAnalog){
				_collected = true;
			}

			//if servo is not collected and dispensed, then a cycle was completed and therefore there is nothing to dispense.
			//the dispensing loop is also stoped and vibration counter reset
			if (_dispensed){
				_dispensed = false;
				_activate = false;
				_vibrationCounter = 0;
			}
		}

	}
}

boolean Dispenser::Activate(){
	if (!_activate){
		_activate = true;
		return true;
	}
	return false;
}

void Dispenser::Setup(boolean calibrate){
	_servo.attach(_servoPin);

	if (calibrate){
		this->AutoCalibrate();
	}
	else{
		_dispansedAnalog = this->ReadEPROM((_dispenserID * 2) - 2);
		//Serial.println(_dispansedAnalog);
		_collectAnalog = this->ReadEPROM((_dispenserID * 2) - 1);
		//Serial.println(_collectAnalog);
		_servo.write(_collectAngle);
	}
}

Servo Dispenser::GetServo(){
	return _servo;
}

void Dispenser::CalibrateDispenseAngle(int collectAngle){
	_collectAngle = collectAngle;
}

void Dispenser::ManualCalibrationServoAnalog(int dispansedAnalog, int collectAnalog){
	_dispansedAnalog = dispansedAnalog;
	_collectAnalog = collectAnalog;
}

boolean Dispenser::AutoCalibrate(){
	//this->ClearEPROM();

	//calibrate dispense and collect angles
	_servo.write(_dispansedAngle);
	delay(3000);
	_dispansedAnalog = analogRead(_anglePin);
	_dispansedAnalog += 50;
	boolean EPROM_0 = this->StoreEPROM(_dispansedAnalog, (_dispenserID * 2) - 2);

	_servo.write(_collectAngle);
	delay(3000);
	_collectAnalog = analogRead(_anglePin);
	_collectAnalog -= 50;
	boolean EPROM_1 = this->StoreEPROM(_collectAnalog, (_dispenserID * 2) - 1);

	return (EPROM_0 && EPROM_1) ? true : false;
}

boolean Dispenser::StoreEPROM(int value, int startAddress) {
	if ((startAddress + 1) * 5 < 512 - 5 && value < 1024) {
		for (int i = startAddress * 5; i < (startAddress + 1) * 5; i++) {
			//Serial.print("Address: ");
			//Serial.println(i);

			if (value > 255) {
				//Serial.print("Value: ");
				//Serial.println(255);
				EEPROM.write(i, 255);
				value -= 255;
			}
			else {
				//Serial.print("Value: ");
				//Serial.println(value);
				EEPROM.write(i, value);
				value = 0;
			}
			delay(10);
		}
		return true;
	}
	else{
		return false;
	}
}

int Dispenser::ReadEPROM(int addressToRead) {
	int value = 0;
	for (int i = addressToRead * 5; i < (addressToRead + 1) * 5; i++) {
		//Serial.print("Address: ");
		//Serial.println(i);
		//Serial.print("Value: ");
		//Serial.println(value);
		value += EEPROM.read(i);
		delay(10);
	}
	return value;
}

void Dispenser::ClearEPROM() {
	for (int i = 0; i < 512; i++)
		EEPROM.write(i, 0);
}