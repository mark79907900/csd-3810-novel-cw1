#include "Vibrator.h"

//Constructor
Vibrator::Vibrator(int vibratorPin){
	_activate = false;
	_vibratorPin = vibratorPin;
}

void Vibrator::Loop(){
	//if the dispenser is stated to dispens
	if (_activate){
		if (millis() - _startTime <= _duration){
			analogWrite(_vibratorPin, _intensity);
		}
		else{
			_intensity = 0L;
			_startTime = 0L;
			_duration = 0L;
			analogWrite(_vibratorPin, _intensity);
			_activate = false;
		}
	}
}

boolean Vibrator::Activate(long duration, int intensity){
	if (!_activate){
		_activate = true;
		_duration = duration;
		_intensity = intensity;
		_startTime = millis();
		return true;
	}
	return false;
}

void Vibrator::Setup(){
	pinMode(_vibratorPin, OUTPUT);
}

