#ifndef Vibrator_h
#define Vibrator_h

//import arduino library
#if defined(ARDUINO) && ARDUINO >= 100
#include <Arduino.h>
#else
#include <WProgram.h>
#include <pins_arduino.h>
#endif


class Vibrator{
private:
	boolean _activate;
	long _duration;
	int _intensity;
	long _startTime;
	int _vibratorPin;

public:

	Vibrator(int vibratorPin);

	/*
	This method should be placed in the loop, it will perform the actions as a background process
	*/
	void Loop();

	/*
	This method is used to dispense an item. 
	It returns true if there was no action being processed already.
	It returns false if the dispenser was already dispensing
	Returns: boolean
	*/
	boolean Activate(long duration, int intensity);

	/*
	This method is used to setup the vibrator pin.
	*/
	void Setup();

};

#endif