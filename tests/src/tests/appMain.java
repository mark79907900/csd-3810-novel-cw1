/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;
import processing.core.*;
import guru.ttslib.*;



/**
 *
 * @author Mark
 */
public class appMain extends PApplet{
    
    TTS tts;
    
    public void setup() {
  size(100,100);
  smooth();
  tts = new TTS();
}

public void draw() {
  background(255);
  fill(255);
  ellipse( 35, 30, 25, 35 );
  ellipse( 65, 30, 25, 35 );
  fill(0);
  ellipse( 40, 35, 10, 10 );
  ellipse( 60, 35, 10, 10 );
  noFill();
  arc(50,50,50,50,0,PI);
  
}

public void mousePressed() {
  tts.setPitchShift(5);
  tts.speak("Hi! I am a speaking Processing sketch");
  
}
}


